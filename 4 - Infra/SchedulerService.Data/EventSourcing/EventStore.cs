﻿using SchedulerService.Domain.Core.Events;
using SchedulerService.Repository.EventSourcing;
using Newtonsoft.Json;
using System;

namespace SchedulerService.Data.EventSourcing
{
    public class EventStore : IEventStore
    {
        private readonly IEventStoreRepository _eventStoreRepository;

        public EventStore(IEventStoreRepository eventStoreRepository)
        {
            _eventStoreRepository = eventStoreRepository;
        }

        public void Save<T>(T theEvent) where T : Event
        {
            var serializedData = JsonConvert.SerializeObject(theEvent);

            var storedEvent = new StoredEvent
            {
                AggregateId = theEvent.AggregateId ?? Guid.NewGuid().ToString(),
                Type = theEvent.Type,
                Action = theEvent.Action,
                Data = serializedData,
                User = "admin"
            };

            _eventStoreRepository.Store(storedEvent);
        }
    }
}