﻿using SchedulerService.Repository.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace SchedulerService.Data.Migrations
{
    public static class Initialize
    {
        public static void Services(IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            scope.ServiceProvider.GetService<SchedulerServiceContext>().Database.Migrate();
        }
    }
}
