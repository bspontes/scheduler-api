﻿using System;
using SchedulerService.Domain.Core.UoW;
using SchedulerService.Repository.Context;

namespace SchedulerService.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SchedulerServiceContext _context;

        public UnitOfWork(SchedulerServiceContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            try
            {
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}