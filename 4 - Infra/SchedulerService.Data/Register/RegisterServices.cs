using SchedulerService.Application.Application;
using SchedulerService.Application.Interface;
using SchedulerService.Data.Bus;
using SchedulerService.Data.EventSourcing;
using SchedulerService.Data.Redis;
using SchedulerService.Data.UoW;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Events;
using SchedulerService.Domain.Core.Notifications;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Core.UoW;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.EventSourcing;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository;
using SchedulerService.Service.CommandHandlers;
using SchedulerService.Service.Interface;
using SchedulerService.Service.QueryHandlers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SchedulerService.Data.Register
{
    public static class RegisterServices
    {
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            Others(services, configuration);
            Applications(services);
            Commands(services);
            Events(services);
            Services(services);
            Mappers(services);
            Repositories(services);
        }

        private static void Applications(IServiceCollection services)
        {
            services.AddScoped<IUserApplication, UserApplication>();
            services.AddScoped<IStoreApplication, StoreApplication>();
            services.AddScoped<IStoreParamsApplication, StoreParamsApplication>();
            services.AddScoped<IStoreBusinessTimeApplication, StoreBusinessTimeApplication>();
            services.AddScoped<IStoreServiceApplication, StoreServiceApplication>();
            services.AddScoped<IStoreImageApplication, StoreImageApplication>();
            services.AddScoped<IBookingApplication, BookingApplication>();
            services.AddScoped<IAddressApplication, AddressApplication>();
            services.AddScoped<IPhoneApplication, PhoneApplication>();
            services.AddScoped<IBackofficeAuthApplication, BackofficeAuthApplication>();
// REPLACE1
        }

        private static void Commands(IServiceCollection services)
        {
            services.AddScoped<IRequestHandler<Domain.Commands.User.InsertCommand, object>, UserCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.User.UpdateCommand, object>, UserCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.User.DeleteCommand, object>, UserCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Store.InsertCommand, object>, StoreCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Store.UpdateCommand, object>, StoreCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Store.DeleteCommand, object>, StoreCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreParams.InsertCommand, object>, StoreParamsCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreParams.UpdateCommand, object>, StoreParamsCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreParams.DeleteCommand, object>, StoreParamsCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreBusinessTime.InsertCommand, object>, StoreBusinessTimeCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreBusinessTime.UpdateCommand, object>, StoreBusinessTimeCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreBusinessTime.DeleteCommand, object>, StoreBusinessTimeCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreService.InsertCommand, object>, StoreServiceCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreService.UpdateCommand, object>, StoreServiceCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreService.DeleteCommand, object>, StoreServiceCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreImage.InsertCommand, object>, StoreImageCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreImage.UpdateCommand, object>, StoreImageCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.StoreImage.DeleteCommand, object>, StoreImageCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Booking.InsertCommand, object>, BookingCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Booking.UpdateCommand, object>, BookingCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Booking.DeleteCommand, object>, BookingCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Address.InsertCommand, object>, AddressCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Address.UpdateCommand, object>, AddressCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Address.DeleteCommand, object>, AddressCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Phone.InsertCommand, object>, PhoneCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Phone.UpdateCommand, object>, PhoneCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.Phone.DeleteCommand, object>, PhoneCommandHandler>();
            services.AddScoped<IRequestHandler<Domain.Commands.BackofficeAuth.LoginCommand, object>, BackofficeAuthCommandHandler>();
// REPLACE2
        }

        private static void Events(IServiceCollection services)
        {
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
        }

        private static void Services(IServiceCollection services)
        {
            services.AddScoped<IUserQueryHandler, UserQueryHandler>();
            services.AddScoped<IStoreQueryHandler, StoreQueryHandler>();
            services.AddScoped<IStoreParamsQueryHandler, StoreParamsQueryHandler>();
            services.AddScoped<IStoreBusinessTimeQueryHandler, StoreBusinessTimeQueryHandler>();
            services.AddScoped<IStoreServiceQueryHandler, StoreServiceQueryHandler>();
            services.AddScoped<IStoreImageQueryHandler, StoreImageQueryHandler>();
            services.AddScoped<IBookingQueryHandler, BookingQueryHandler>();
            services.AddScoped<IAddressQueryHandler, AddressQueryHandler>();
            services.AddScoped<IPhoneQueryHandler, PhoneQueryHandler>();
// REPLACE3
        }

        private static void Mappers(IServiceCollection services)
        {
            services.AddScoped<UserMapper>();
            services.AddScoped<StoreMapper>();
            services.AddScoped<StoreParamsMapper>();
            services.AddScoped<StoreBusinessTimeMapper>();
            services.AddScoped<StoreServiceMapper>();
            services.AddScoped<StoreImageMapper>();
            services.AddScoped<BookingMapper>();
            services.AddScoped<AddressMapper>();
            services.AddScoped<PhoneMapper>();
// REPLACE4
        }

        private static void Repositories(IServiceCollection services)
        {
            services.AddScoped<IEventStoreRepository, EventStoreRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IStoreRepository, StoreRepository>();
            services.AddScoped<IStoreParamsRepository, StoreParamsRepository>();
            services.AddScoped<IStoreBusinessTimeRepository, StoreBusinessTimeRepository>();
            services.AddScoped<IStoreServiceRepository, StoreServiceRepository>();
            services.AddScoped<IStoreImageRepository, StoreImageRepository>();
            services.AddScoped<IBookingRepository, BookingRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IPhoneRepository, PhoneRepository>();
// REPLACE5
        }

        private static void Others(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SchedulerServiceContext>(options => options.EnableSensitiveDataLogging().UseSqlServer(configuration["ConnectionStrings:DefaultConnection"]), ServiceLifetime.Scoped);

            services.AddScoped<IMediatorHandler, InMemoryBus>();
            services.AddScoped<IEventStore, EventStore>();
            services.AddScoped<IRedisRepository, RedisRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
