﻿using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Core.Settings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Data.Redis
{
    public class RedisRepository : IRedisRepository
    {
        private IDatabase _db;
        private ConnectionMultiplexer _redis;
        private readonly IOptions<RedisSettings> _settings;

        public RedisRepository(IOptions<RedisSettings> settings)
        {
            _settings = settings;
        }

        private void Connect()
        {
            try
            {
                var co = new ConfigurationOptions
                {
                    SyncTimeout = _settings.Value.SyncTimeout,
                    ConnectTimeout = _settings.Value.ConnectTimeout,
                    EndPoints = { _settings.Value.Host },
                    AbortOnConnectFail = false
                };

                _redis = ConnectionMultiplexer.Connect(co);
                _db = _redis.GetDatabase();
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task Delete(string identifier)
        {
            try
            {
                if (_db == null || !_db.IsConnected(identifier)) Connect();
                if (_db.KeyExists(identifier)) await _db.KeyDeleteAsync(identifier);
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task<T> Get<T>(string identifier)
        {
            try
            {
                if (_db == null || !_db.IsConnected(identifier)) Connect();

                var value = await _db.StringGetAsync(identifier);

                if (string.IsNullOrEmpty(value)) return default;

                var ret = JsonConvert.DeserializeObject<T>(value);

                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task<List<T>> GetList<T>(string identifier)
        {
            try
            {
                if (_db == null || !_db.IsConnected(identifier)) Connect();

                var value = await _db.StringGetAsync(identifier);

                if (string.IsNullOrEmpty(value)) return new List<T>();

                var ret = JsonConvert.DeserializeObject<List<T>>(value);

                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task<BasePagination<T>> GetListPagination<T>(string identifier)
        {
            try
            {
                if (_db == null || !_db.IsConnected(identifier)) Connect();

                var value = await _db.StringGetAsync(identifier);

                if (string.IsNullOrEmpty(value)) return new BasePagination<T>();

                var ret = JsonConvert.DeserializeObject<BasePagination<T>>(value);

                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task Save<T>(string identifier, T obj, DateTime? expiration = null)
        {
            try
            {
                if (obj == null) return;
                if (_db == null || !_db.IsConnected(identifier)) Connect();

                await Delete(identifier);

                expiration ??= DateTime.Now.AddDays(1);

                var timeExpire = expiration.Value.Subtract(DateTime.Now);
                var valueObj = JsonConvert.SerializeObject(obj);

                await _db.StringSetAsync(identifier, valueObj, timeExpire);
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task<bool> ContainsKey(string identifier)
        {
            try
            {
                if (_db == null || !_db.IsConnected(identifier)) Connect();

                var value = await _db.StringGetAsync(identifier);

                var ret = !string.IsNullOrEmpty(value);

                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task<DateTime> GetLastUpdate(string identifier)
        {
            try
            {
                if (_db == null || !_db.IsConnected(identifier)) Connect();

                var value = await _db.StringGetAsync(identifier);

                if (string.IsNullOrEmpty(value)) return DateTime.UtcNow;

                var ret = JsonConvert.DeserializeObject<DateTime>(value);

                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }

        public async Task SaveLastUpdate(string identifier, DateTime? expiration = null, DateTime? lasUpdate = null)
        {
            try
            {
                if (_db == null || !_db.IsConnected(identifier)) Connect();

                await Delete(identifier);

                expiration ??= DateTime.Now.AddDays(1);
                lasUpdate ??= DateTime.Now;

                var timeExpire = expiration.Value.Subtract(DateTime.Now);
                var valueObj = JsonConvert.SerializeObject(lasUpdate);

                await _db.StringSetAsync(identifier, valueObj, timeExpire);
            }
            catch (Exception ex)
            {
                throw new Exception($"{GetType().FullName} - {ex.Message}");
            }
        }
    }
}
