﻿using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Core.Events;
using MediatR;
using System;
using System.Threading.Tasks;

namespace SchedulerService.Data.Bus
{
    public sealed class InMemoryBus : IMediatorHandler
    {
        private readonly IMediator _mediator;
        private readonly IEventStore _eventStore;

        public InMemoryBus(IEventStore eventStore, IMediator mediator)
        {
            _eventStore = eventStore;
            _mediator = mediator;
        }

        public Task<object> SendCommand<T>(T command) where T : Command
        {
            return _mediator.Send(command);
        }

        public Task RaiseEvent<T>(T @event) where T : Event
        {
            try
            {
                _eventStore?.Save(@event);

                return _mediator.Publish(@event);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}