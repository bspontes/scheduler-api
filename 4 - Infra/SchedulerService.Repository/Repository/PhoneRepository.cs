using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class PhoneRepository : BaseRepository<Phone>, IPhoneRepository
    {
        private SchedulerServiceContext _context;

        public PhoneRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
