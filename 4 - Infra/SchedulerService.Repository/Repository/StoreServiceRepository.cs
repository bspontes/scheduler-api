using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class StoreServiceRepository : BaseRepository<StoreService>, IStoreServiceRepository
    {
        private SchedulerServiceContext _context;

        public StoreServiceRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
