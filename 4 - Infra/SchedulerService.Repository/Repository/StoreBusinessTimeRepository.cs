using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class StoreBusinessTimeRepository : BaseRepository<StoreBusinessTime>, IStoreBusinessTimeRepository
    {
        private SchedulerServiceContext _context;

        public StoreBusinessTimeRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
