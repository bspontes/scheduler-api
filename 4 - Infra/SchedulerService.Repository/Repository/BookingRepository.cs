using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class BookingRepository : BaseRepository<Booking>, IBookingRepository
    {
        private SchedulerServiceContext _context;

        public BookingRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
