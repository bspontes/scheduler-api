using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class StoreParamsRepository : BaseRepository<StoreParams>, IStoreParamsRepository
    {
        private SchedulerServiceContext _context;

        public StoreParamsRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
