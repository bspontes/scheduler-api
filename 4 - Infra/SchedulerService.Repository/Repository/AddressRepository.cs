using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class AddressRepository : BaseRepository<Address>, IAddressRepository
    {
        private SchedulerServiceContext _context;

        public AddressRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
