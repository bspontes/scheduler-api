﻿using SchedulerService.Domain.Common;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchedulerService.Repository.Repository.Common
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly SchedulerServiceContext _context;

        public BaseRepository(SchedulerServiceContext context)
        {
            _context = context;
        }

        public async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> filter, List<string> includes = null)
        {
            try
            {
                var find = _context.Set<TEntity>().AsQueryable();

                if (includes != null)
                {
                    foreach (var item in includes)
                    {
                        find = find.Include(item);
                    }
                }

                return await find.FirstOrDefaultAsync(filter);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public TEntity Find(Expression<Func<TEntity, bool>> filter, List<string> includes = null)
        {
            try
            {
                var find = _context.Set<TEntity>().AsQueryable();

                if (includes != null)
                {
                    foreach (var item in includes)
                    {
                        find = find.Include(item);
                    }
                }

                return find.FirstOrDefault(filter);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter, List<string> includes = null)
        {
            var any = _context.Set<TEntity>().AsQueryable();

            if (includes != null)
            {
                any = includes.Aggregate(any, (current, item) => current.Include(item));
            }

            return await any.AnyAsync(filter);
        }

        public async Task<List<TEntity>> ListAsync(List<string> includes = null)
        {
            var list = _context.Set<TEntity>().AsQueryable();

            if (includes != null)
            {
                list = includes.Aggregate(list, (current, item) => current.Include(item));
            }

            return await list.ToListAsync();
        }

        public async Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter, List<string> includes = null)
        {
            try
            {
                var list = _context.Set<TEntity>().AsQueryable();

                if (includes != null)
                {
                    list = includes.Aggregate(list, (current, item) => current.Include(item));
                }

                return await list.Where(filter).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter, BaseSortingRequest sorting, BasePaginationRequest pagination, List<string> includes = null)
        {
            try
            {
                var list = _context.Set<TEntity>().AsQueryable();

                if (includes != null)
                {
                    list = includes.Aggregate(list, (current, item) => current.Include(item));
                }

                var result = list.Where(filter)
                    .OrderBy($"{sorting.Field} {sorting.TypeSorting}")
                    .Skip(pagination.Page)
                    .Take(pagination.PageSize);

                return await result.ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task Insert(TEntity model)
        {
            try
            {
                _context.Set<TEntity>().Add(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Task.CompletedTask;
        }

        public Task Insert(IEnumerable<TEntity> models)
        {
            try
            {
                _context.Set<TEntity>().AddRange(models);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Task.CompletedTask;
        }

        public Task Update(TEntity model)
        {
            try
            {
                _context.Set<TEntity>().Attach(model);
                _context.Entry(model).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Task.CompletedTask;
        }

        public Task Update(IEnumerable<TEntity> models)
        {
            try
            {
                _context.Set<TEntity>().AttachRange(models);
                _context.Entry(models).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Task.CompletedTask;
        }

        public Task Delete(TEntity model)
        {
            try
            {
                _context.Set<TEntity>().Remove(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Task.CompletedTask;
        }

        public Task Delete(string id)
        {
            try
            {
                var del = _context.Set<TEntity>().Find(id);
                if (del != null) _context.Set<TEntity>().Remove(del);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Task.CompletedTask;
        }

        public Task Delete(IEnumerable<TEntity> models)
        {
            try
            {
                _context.Set<TEntity>().RemoveRange(models);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Task.CompletedTask;
        }

        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> filter)
        {
            try
            {
                var list = _context.Set<TEntity>().AsQueryable();

                var result = await list.CountAsync(filter);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> CountAsync()
        {
            int count;

            try
            {
                count = await _context.Set<TEntity>().CountAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return count;
        }
    }
}
