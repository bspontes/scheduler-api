using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class StoreRepository : BaseRepository<Store>, IStoreRepository
    {
        private SchedulerServiceContext _context;

        public StoreRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
