using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class StoreImageRepository : BaseRepository<StoreImage>, IStoreImageRepository
    {
        private SchedulerServiceContext _context;

        public StoreImageRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
