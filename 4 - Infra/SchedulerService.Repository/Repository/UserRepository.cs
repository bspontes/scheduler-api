using SchedulerService.Domain.Model;
using SchedulerService.Repository.Context;
using SchedulerService.Repository.Interface;
using SchedulerService.Repository.Repository.Common;

namespace SchedulerService.Repository.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private SchedulerServiceContext _context;

        public UserRepository(SchedulerServiceContext context) : base(context)
        {
            _context = context;
        }
    }
}
