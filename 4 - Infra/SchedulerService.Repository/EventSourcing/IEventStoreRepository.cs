﻿using System;
using System.Collections.Generic;
using SchedulerService.Domain.Core.Events;

namespace SchedulerService.Repository.EventSourcing
{
    public interface IEventStoreRepository : IDisposable
    {
        void Store(StoredEvent theEvent);

        IList<StoredEvent> All(string aggregateId);
    }
}