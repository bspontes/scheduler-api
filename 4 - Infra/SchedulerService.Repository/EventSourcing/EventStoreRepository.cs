﻿using SchedulerService.Domain.Core.Events;
using SchedulerService.Repository.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SchedulerService.Repository.EventSourcing
{
    public class EventStoreRepository : IEventStoreRepository
    {
        //private readonly EventStoreContext _context;

        //public EventStoreRepository(EventStoreContext context)
        //{
        //    _context = context;
        //}

        public IList<StoredEvent> All(string aggregateId)
        {
            //return (from e in _context.StoredEvent where e.AggregateId == aggregateId select e).ToList();
            return null;
        }

        public void Store(StoredEvent theEvent)
        {
            try
            {
                //_context.StoredEvent.Add(theEvent);
                //_context.SaveChanges();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Dispose()
        {
            //_context.Dispose();
        }
    }
}