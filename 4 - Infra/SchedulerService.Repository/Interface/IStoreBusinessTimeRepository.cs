using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IStoreBusinessTimeRepository : IBaseRepository<StoreBusinessTime>
    {
    }
}
