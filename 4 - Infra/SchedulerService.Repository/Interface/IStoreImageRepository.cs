using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IStoreImageRepository : IBaseRepository<StoreImage>
    {
    }
}
