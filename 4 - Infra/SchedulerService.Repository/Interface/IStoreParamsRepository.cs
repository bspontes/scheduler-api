using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IStoreParamsRepository : IBaseRepository<StoreParams>
    {
    }
}
