﻿using SchedulerService.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SchedulerService.Repository.Interface
{
    public interface IBaseRepository<TEntity>
    {
        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> filter, List<string> includes = null);

        TEntity Find(Expression<Func<TEntity, bool>> filter, List<string> includes = null);

        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> filter, List<string> includes = null);

        Task<List<TEntity>> ListAsync(List<string> includes = null);

        Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter, List<string> includes = null);

        Task<List<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter, BaseSortingRequest sorting, BasePaginationRequest pagination, List<string> includes = null);

        Task Insert(TEntity model);

        Task Insert(IEnumerable<TEntity> models);

        Task Update(TEntity model);

        Task Update(IEnumerable<TEntity> models);

        Task Delete(TEntity model);

        Task Delete(IEnumerable<TEntity> models);

        Task Delete(string id);

        Task<int> CountAsync(Expression<Func<TEntity, bool>> filter);

        Task<int> CountAsync();
    }
}
