using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IUserRepository : IBaseRepository<User>
    {
    }
}
