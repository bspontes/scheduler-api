using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IPhoneRepository : IBaseRepository<Phone>
    {
    }
}
