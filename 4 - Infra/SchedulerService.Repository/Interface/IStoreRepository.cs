using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IStoreRepository : IBaseRepository<Store>
    {
    }
}
