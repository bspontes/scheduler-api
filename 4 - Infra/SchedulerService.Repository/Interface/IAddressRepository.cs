using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IAddressRepository : IBaseRepository<Address>
    {
    }
}
