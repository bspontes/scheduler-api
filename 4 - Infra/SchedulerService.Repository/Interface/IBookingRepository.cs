using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IBookingRepository : IBaseRepository<Booking>
    {
    }
}
