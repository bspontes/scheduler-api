using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Interface
{
    public interface IStoreServiceRepository : IBaseRepository<StoreService>
    {
    }
}
