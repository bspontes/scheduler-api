﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace SchedulerService.Repository.Context
{
    public class SchedulerServiceContextFactory : IDesignTimeDbContextFactory<SchedulerServiceContext>
    {
        public SchedulerServiceContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SchedulerServiceContext>();
            optionsBuilder.UseSqlServer(args.ToString());

            return new SchedulerServiceContext(optionsBuilder.Options);
        }
    }
}
