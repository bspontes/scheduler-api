using Microsoft.EntityFrameworkCore;
using SchedulerService.Domain.Model;
using SchedulerService.Repository.Mappings;

namespace SchedulerService.Repository.Context
{
    public class SchedulerServiceContext : DbContext
    {
        public SchedulerServiceContext(DbContextOptions<SchedulerServiceContext> options) : base(options)
        {
        }

        public virtual DbSet<User> User { get; set; }

        public virtual DbSet<Store> Store { get; set; }

        public virtual DbSet<StoreParams> StoreParams { get; set; }

        public virtual DbSet<StoreBusinessTime> StoreBusinessTime { get; set; }

        public virtual DbSet<StoreService> StoreService { get; set; }

        public virtual DbSet<StoreImage> StoreImage { get; set; }

        public virtual DbSet<Booking> Booking { get; set; }

        public virtual DbSet<Address> Address { get; set; }

        public virtual DbSet<Phone> Phone { get; set; }
// REPLACE1

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new StoreMap());
            modelBuilder.ApplyConfiguration(new StoreParamsMap());
            modelBuilder.ApplyConfiguration(new StoreBusinessTimeMap());
            modelBuilder.ApplyConfiguration(new StoreServiceMap());
            modelBuilder.ApplyConfiguration(new StoreImageMap());
            modelBuilder.ApplyConfiguration(new BookingMap());
            modelBuilder.ApplyConfiguration(new AddressMap());
            modelBuilder.ApplyConfiguration(new PhoneMap());
            modelBuilder.ApplyConfiguration(new StorePhoneMap());
            modelBuilder.ApplyConfiguration(new StoreAddressMap());
            modelBuilder.ApplyConfiguration(new UserPhoneMap());
// REPLACE2

            base.OnModelCreating(modelBuilder);
        }
    }
}
