using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Password)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Document)
                .IsRequired()
                .HasMaxLength(15);

            builder.Property(x => x.Sex)
                .IsRequired()
                .HasMaxLength(20)
                .HasConversion<string>();

            builder.Property(x => x.Profession)
                .HasMaxLength(100);

            builder.Property(x => x.Status)
                .IsRequired()
                .HasMaxLength(100)
                .HasConversion<string>();

            builder.Property(x => x.ProfileUrl)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.Type)
                .IsRequired()
                .HasMaxLength(100)
                .HasConversion<string>();

            builder.Property(x => x.Origin)
                .IsRequired()
                .HasMaxLength(100)
                .HasConversion<string>();

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();

        }
    }
}
