using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Mappings
{
    public class StorePhoneMap : IEntityTypeConfiguration<StorePhone>
    {
        public void Configure(EntityTypeBuilder<StorePhone> builder)
        {
            builder.HasKey(x => new { x.StoreId, x.PhoneId });

            builder.HasOne(x => x.Store)
                .WithMany(x => x.StorePhones)
                .HasForeignKey(x => x.StoreId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Phone)
                .WithMany(x => x.StorePhones)
                .HasForeignKey(x => x.PhoneId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
