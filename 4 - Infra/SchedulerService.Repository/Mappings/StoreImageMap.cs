using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class StoreImageMap : IEntityTypeConfiguration<StoreImage>
    {
        public void Configure(EntityTypeBuilder<StoreImage> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.OriginalUrl)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.ThumbnailUrl)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.StoreId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();

            builder.HasOne(x => x.Store)
                .WithMany(x => x.Images)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
