using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class StoreServiceMap : IEntityTypeConfiguration<StoreService>
    {
        public void Configure(EntityTypeBuilder<StoreService> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Type)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Duration)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Price)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.PriceType)
                .IsRequired()
                .HasMaxLength(100)
                .HasConversion<string>();

            builder.Property(x => x.StoreId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();

            builder.HasOne(x => x.Store)
                .WithMany(x => x.Services)
                .OnDelete(DeleteBehavior.NoAction);

        }
    }
}
