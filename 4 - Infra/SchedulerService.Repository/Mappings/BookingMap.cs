using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class BookingMap : IEntityTypeConfiguration<Booking>
    {
        public void Configure(EntityTypeBuilder<Booking> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.UserId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.StoreUserId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.StoreId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.StoreServiceId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.Day)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.StartTime)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.EndTime)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.Status)
                .IsRequired()
                .HasMaxLength(100)
                .HasConversion<string>();

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();

        }
    }
}
