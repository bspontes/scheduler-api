using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class AddressMap : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.Zipcode)
                .IsRequired()
                .HasMaxLength(10);

            builder.Property(x => x.Street)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(x => x.Number)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(x => x.Complement)
                .HasMaxLength(255);

            builder.Property(x => x.Neighborhood)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(x => x.City)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(x => x.State)
                .IsRequired()
                .HasMaxLength(2);

            builder.Property(x => x.Country)
                .IsRequired()
                .HasMaxLength(5);

            builder.Property(x => x.Latitude)
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(x => x.Longitude)
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();


        }
    }
}
