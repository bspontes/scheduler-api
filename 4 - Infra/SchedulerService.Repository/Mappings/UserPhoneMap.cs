using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Mappings
{
    public class UserPhoneMap : IEntityTypeConfiguration<UserPhone>
    {
        public void Configure(EntityTypeBuilder<UserPhone> builder)
        {
            builder.HasKey(x => new { x.UserId, x.PhoneId });

            builder.HasOne(x => x.User)
                .WithMany(x => x.UserPhones)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Phone)
                .WithMany(x => x.UserPhones)
                .HasForeignKey(x => x.PhoneId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
