using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class StoreBusinessTimeMap : IEntityTypeConfiguration<StoreBusinessTime>
    {
        public void Configure(EntityTypeBuilder<StoreBusinessTime> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.DayOfWeek)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(x => x.StartTime)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.LunchTime)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.EndTime)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.StoreId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();

            builder.HasOne(x => x.Store)
                .WithMany(x => x.BusinessTimes)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
