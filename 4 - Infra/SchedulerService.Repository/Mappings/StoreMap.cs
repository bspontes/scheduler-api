using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class StoreMap : IEntityTypeConfiguration<Store>
    {
        public void Configure(EntityTypeBuilder<Store> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.FantasyName)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(100);

            builder.Property(x => x.Document)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(x => x.BusinessLine)
                .IsRequired()
                .HasMaxLength(100)
                .HasConversion<string>();

            builder.Property(x => x.Status)
                .IsRequired()
                .HasMaxLength(100)
                .HasConversion<string>();

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();

            builder.HasOne(x => x.Params)
                .WithOne(x => x.Store)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(x => x.BusinessTimes)
                .WithOne(x => x.Store)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(x => x.Services)
                .WithOne(x => x.Store)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(x => x.Images)
                .WithOne(x => x.Store)
                .OnDelete(DeleteBehavior.NoAction);

        }
    }
}
