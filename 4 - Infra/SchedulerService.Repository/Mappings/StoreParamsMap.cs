using SchedulerService.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SchedulerService.Repository.Mappings
{
    public class StoreParamsMap : IEntityTypeConfiguration<StoreParams>
    {
        public void Configure(EntityTypeBuilder<StoreParams> builder)
        {
            builder.Property(x => x.Id)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.Description)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.LogoUrl)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.BackgroundUrl)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.BaseColor)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(x => x.TextColor)
                .IsRequired()
                .HasMaxLength(20);

            builder.Property(x => x.StoreId)
                .IsRequired()
                .HasMaxLength(40);

            builder.Property(x => x.CreatedAt)
                .IsRequired();

            builder.Property(x => x.UpdatedAt)
                .IsRequired();

            builder.HasOne(x => x.Store)
                .WithOne(x => x.Params)
                .OnDelete(DeleteBehavior.NoAction);

        }
    }
}
