using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SchedulerService.Domain.Model;

namespace SchedulerService.Repository.Mappings
{
    public class StoreAddressMap : IEntityTypeConfiguration<StoreAddress>
    {
        public void Configure(EntityTypeBuilder<StoreAddress> builder)
        {
            builder.HasKey(x => new { x.StoreId, x.AddressId });

            builder.HasOne(x => x.Store)
                .WithMany(x => x.StoreAddresses)
                .HasForeignKey(x => x.StoreId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Address)
                .WithMany(x => x.StoreAddresses)
                .HasForeignKey(x => x.AddressId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
