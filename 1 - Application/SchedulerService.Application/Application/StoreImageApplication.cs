using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.StoreImage;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class StoreImageApplication : IStoreImageApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IStoreImageQueryHandler _query;
        private readonly StoreImageMapper _mapper;

        public StoreImageApplication(IMediatorHandler mediator, IStoreImageQueryHandler query, StoreImageMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<StoreImageViewModel>>> List(StoreImageFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<StoreImageViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreImageViewModel>> FindById(string id)
        {
            var response = new BaseReturn<StoreImageViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreImageViewModel>> Insert(StoreImageViewModel viewModel)
        {
            var response = new BaseReturn<StoreImageViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.OriginalUrl,
                    viewModel.ThumbnailUrl,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreImageViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreImageViewModel>> Update(StoreImageViewModel viewModel, string id)
        {
            var response = new BaseReturn<StoreImageViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.OriginalUrl,
                    viewModel.ThumbnailUrl,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreImageViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreImageViewModel>> Delete(string id)
        {
            var response = new BaseReturn<StoreImageViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreImageViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<StoreImageViewModel>>> Download(StoreImageFilter filter)
        {
            var response = new BaseReturn<List<StoreImageViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
