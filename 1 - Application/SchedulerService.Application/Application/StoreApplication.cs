using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.Store;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class StoreApplication : IStoreApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IStoreQueryHandler _query;
        private readonly StoreMapper _mapper;

        public StoreApplication(IMediatorHandler mediator, IStoreQueryHandler query, StoreMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<StoreViewModel>>> List(StoreFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<StoreViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreViewModel>> FindById(string id)
        {
            var response = new BaseReturn<StoreViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreViewModel>> Insert(StoreViewModel viewModel)
        {
            var response = new BaseReturn<StoreViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.Name,
                    viewModel.FantasyName,
                    viewModel.Email,
                    viewModel.Document,
                    viewModel.BusinessLine,
                    viewModel.Status
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreViewModel>> Update(StoreViewModel viewModel, string id)
        {
            var response = new BaseReturn<StoreViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.Name,
                    viewModel.FantasyName,
                    viewModel.Email,
                    viewModel.Document,
                    viewModel.BusinessLine,
                    viewModel.Status
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreViewModel>> Delete(string id)
        {
            var response = new BaseReturn<StoreViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<StoreViewModel>>> Download(StoreFilter filter)
        {
            var response = new BaseReturn<List<StoreViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
