using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.User;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class UserApplication : IUserApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IUserQueryHandler _query;
        private readonly UserMapper _mapper;

        public UserApplication(IMediatorHandler mediator, IUserQueryHandler query, UserMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<UserViewModel>>> List(UserFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<UserViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<UserViewModel>> FindById(string id)
        {
            var response = new BaseReturn<UserViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<UserViewModel>> Insert(UserViewModel viewModel)
        {
            var response = new BaseReturn<UserViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.Name,
                    viewModel.Password,
                    viewModel.Email,
                    viewModel.Document,
                    viewModel.Sex,
                    viewModel.Profession,
                    viewModel.Status,
                    viewModel.ProfileUrl,
                    viewModel.Type,
                    viewModel.Origin
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (UserViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<UserViewModel>> Update(UserViewModel viewModel, string id)
        {
            var response = new BaseReturn<UserViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.Name,
                    viewModel.Password,
                    viewModel.Email,
                    viewModel.Document,
                    viewModel.Sex,
                    viewModel.Profession,
                    viewModel.Status,
                    viewModel.ProfileUrl,
                    viewModel.Type,
                    viewModel.Origin
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (UserViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<UserViewModel>> Delete(string id)
        {
            var response = new BaseReturn<UserViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (UserViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<UserViewModel>>> Download(UserFilter filter)
        {
            var response = new BaseReturn<List<UserViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
