using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.Address;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class AddressApplication : IAddressApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IAddressQueryHandler _query;
        private readonly AddressMapper _mapper;

        public AddressApplication(IMediatorHandler mediator, IAddressQueryHandler query, AddressMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<AddressViewModel>>> List(AddressFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<AddressViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<AddressViewModel>> FindById(string id)
        {
            var response = new BaseReturn<AddressViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<AddressViewModel>> Insert(AddressViewModel viewModel)
        {
            var response = new BaseReturn<AddressViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.Zipcode,
                    viewModel.Street,
                    viewModel.Number,
                    viewModel.Complement,
                    viewModel.Neighborhood,
                    viewModel.City,
                    viewModel.State,
                    viewModel.Country,
                    viewModel.Latitude,
                    viewModel.Longitude
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (AddressViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<AddressViewModel>> Update(AddressViewModel viewModel, string id)
        {
            var response = new BaseReturn<AddressViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.Zipcode,
                    viewModel.Street,
                    viewModel.Number,
                    viewModel.Complement,
                    viewModel.Neighborhood,
                    viewModel.City,
                    viewModel.State,
                    viewModel.Country,
                    viewModel.Latitude,
                    viewModel.Longitude
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (AddressViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<AddressViewModel>> Delete(string id)
        {
            var response = new BaseReturn<AddressViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (AddressViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<AddressViewModel>>> Download(AddressFilter filter)
        {
            var response = new BaseReturn<List<AddressViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
