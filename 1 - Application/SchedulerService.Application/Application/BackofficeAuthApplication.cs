using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.BackofficeAuth;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.ViewModel;
using System;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class BackofficeAuthApplication : IBackofficeAuthApplication
    {
        private readonly IMediatorHandler _mediator;

        public BackofficeAuthApplication(IMediatorHandler mediator)
        {
            _mediator = mediator;
        }

        public async Task<BaseReturn<UserViewModel>> Login(BackofficeLoginViewModel viewModel)
        {
            var response = new BaseReturn<UserViewModel>();

            try
            {
                var command = new LoginCommand(
                    viewModel.Email,
                    viewModel.Password,
                    UserTypeEnum.BACKOFFICE
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (UserViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
