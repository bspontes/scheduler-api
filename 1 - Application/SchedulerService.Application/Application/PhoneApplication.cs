using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.Phone;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class PhoneApplication : IPhoneApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IPhoneQueryHandler _query;
        private readonly PhoneMapper _mapper;

        public PhoneApplication(IMediatorHandler mediator, IPhoneQueryHandler query, PhoneMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<PhoneViewModel>>> List(PhoneFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<PhoneViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<PhoneViewModel>> FindById(string id)
        {
            var response = new BaseReturn<PhoneViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<PhoneViewModel>> Insert(PhoneViewModel viewModel)
        {
            var response = new BaseReturn<PhoneViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.CountryCode,
                    viewModel.AreaCode,
                    viewModel.Number
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (PhoneViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<PhoneViewModel>> Update(PhoneViewModel viewModel, string id)
        {
            var response = new BaseReturn<PhoneViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.CountryCode,
                    viewModel.AreaCode,
                    viewModel.Number
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (PhoneViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<PhoneViewModel>> Delete(string id)
        {
            var response = new BaseReturn<PhoneViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (PhoneViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<PhoneViewModel>>> Download(PhoneFilter filter)
        {
            var response = new BaseReturn<List<PhoneViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
