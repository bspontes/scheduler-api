using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.Booking;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class BookingApplication : IBookingApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IBookingQueryHandler _query;
        private readonly BookingMapper _mapper;

        public BookingApplication(IMediatorHandler mediator, IBookingQueryHandler query, BookingMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<BookingViewModel>>> List(BookingFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<BookingViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<BookingViewModel>> FindById(string id)
        {
            var response = new BaseReturn<BookingViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<BookingViewModel>> Insert(BookingViewModel viewModel)
        {
            var response = new BaseReturn<BookingViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.Day,
                    viewModel.StartTime,
                    viewModel.EndTime
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (BookingViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<BookingViewModel>> Update(BookingViewModel viewModel, string id)
        {
            var response = new BaseReturn<BookingViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.UserId,
                    viewModel.StoreUserId,
                    viewModel.StoreId,
                    viewModel.StoreServiceId,
                    viewModel.Day,
                    viewModel.StartTime,
                    viewModel.EndTime
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (BookingViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<BookingViewModel>> Delete(string id)
        {
            var response = new BaseReturn<BookingViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (BookingViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<BookingViewModel>>> Download(BookingFilter filter)
        {
            var response = new BaseReturn<List<BookingViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
