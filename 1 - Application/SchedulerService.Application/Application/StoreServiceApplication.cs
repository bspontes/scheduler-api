using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.StoreService;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class StoreServiceApplication : IStoreServiceApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IStoreServiceQueryHandler _query;
        private readonly StoreServiceMapper _mapper;

        public StoreServiceApplication(IMediatorHandler mediator, IStoreServiceQueryHandler query, StoreServiceMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<StoreServiceViewModel>>> List(StoreServiceFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<StoreServiceViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreServiceViewModel>> FindById(string id)
        {
            var response = new BaseReturn<StoreServiceViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreServiceViewModel>> Insert(StoreServiceViewModel viewModel)
        {
            var response = new BaseReturn<StoreServiceViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.Name,
                    viewModel.Type,
                    viewModel.Duration,
                    viewModel.Price,
                    viewModel.PriceType,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreServiceViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreServiceViewModel>> Update(StoreServiceViewModel viewModel, string id)
        {
            var response = new BaseReturn<StoreServiceViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.Name,
                    viewModel.Type,
                    viewModel.Duration,
                    viewModel.Price,
                    viewModel.PriceType,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreServiceViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreServiceViewModel>> Delete(string id)
        {
            var response = new BaseReturn<StoreServiceViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreServiceViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<StoreServiceViewModel>>> Download(StoreServiceFilter filter)
        {
            var response = new BaseReturn<List<StoreServiceViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
