using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.StoreParams;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class StoreParamsApplication : IStoreParamsApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IStoreParamsQueryHandler _query;
        private readonly StoreParamsMapper _mapper;

        public StoreParamsApplication(IMediatorHandler mediator, IStoreParamsQueryHandler query, StoreParamsMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<StoreParamsViewModel>>> List(StoreParamsFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<StoreParamsViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreParamsViewModel>> FindById(string id)
        {
            var response = new BaseReturn<StoreParamsViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreParamsViewModel>> Insert(StoreParamsViewModel viewModel)
        {
            var response = new BaseReturn<StoreParamsViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.Description,
                    viewModel.LogoUrl,
                    viewModel.BackgroundUrl,
                    viewModel.BaseColor,
                    viewModel.TextColor,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreParamsViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreParamsViewModel>> Update(StoreParamsViewModel viewModel, string id)
        {
            var response = new BaseReturn<StoreParamsViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.Description,
                    viewModel.LogoUrl,
                    viewModel.BackgroundUrl,
                    viewModel.BaseColor,
                    viewModel.TextColor,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreParamsViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreParamsViewModel>> Delete(string id)
        {
            var response = new BaseReturn<StoreParamsViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreParamsViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<StoreParamsViewModel>>> Download(StoreParamsFilter filter)
        {
            var response = new BaseReturn<List<StoreParamsViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
