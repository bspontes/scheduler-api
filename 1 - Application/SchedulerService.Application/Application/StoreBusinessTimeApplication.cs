using SchedulerService.Application.Interface;
using SchedulerService.Domain.Commands.StoreBusinessTime;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.ViewModel;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Application
{
    public class StoreBusinessTimeApplication : IStoreBusinessTimeApplication
    {
        private readonly IMediatorHandler _mediator;
        private readonly IStoreBusinessTimeQueryHandler _query;
        private readonly StoreBusinessTimeMapper _mapper;

        public StoreBusinessTimeApplication(IMediatorHandler mediator, IStoreBusinessTimeQueryHandler query, StoreBusinessTimeMapper mapper)
        {
            _mediator = mediator;
            _query = query;
            _mapper = mapper;
        }

        public async Task<BasePaginationReturn<List<StoreBusinessTimeViewModel>>> List(StoreBusinessTimeFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<StoreBusinessTimeViewModel>>();

            try
            {
                var service = await _query.List(filter, sorting, pagination);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreBusinessTimeViewModel>> FindById(string id)
        {
            var response = new BaseReturn<StoreBusinessTimeViewModel>();

            try
            {
                var service = await _query.FindById(id);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreBusinessTimeViewModel>> Insert(StoreBusinessTimeViewModel viewModel)
        {
            var response = new BaseReturn<StoreBusinessTimeViewModel>();

            try
            {
                var command = new InsertCommand(
                    viewModel.DayOfWeek,
                    viewModel.StartTime,
                    viewModel.LunchTime,
                    viewModel.EndTime,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreBusinessTimeViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreBusinessTimeViewModel>> Update(StoreBusinessTimeViewModel viewModel, string id)
        {
            var response = new BaseReturn<StoreBusinessTimeViewModel>();

            try
            {
                var command = new UpdateCommand(
                    id,
                    viewModel.DayOfWeek,
                    viewModel.StartTime,
                    viewModel.LunchTime,
                    viewModel.EndTime,
                    viewModel.StoreId
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreBusinessTimeViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreBusinessTimeViewModel>> Delete(string id)
        {
            var response = new BaseReturn<StoreBusinessTimeViewModel>();

            try
            {
                var command = new DeleteCommand(
                    id
                    );

                var data = await _mediator.SendCommand(command);

                response.Data = (StoreBusinessTimeViewModel)data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<StoreBusinessTimeViewModel>>> Download(StoreBusinessTimeFilter filter)
        {
            var response = new BaseReturn<List<StoreBusinessTimeViewModel>>();

            try
            {
                var service = await _query.Download(filter);

                var viewModel = _mapper.ViewModel(service.Data);

                response.Data = viewModel;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
