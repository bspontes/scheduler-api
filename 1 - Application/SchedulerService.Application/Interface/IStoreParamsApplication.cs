using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IStoreParamsApplication
    {
        Task<BasePaginationReturn<List<StoreParamsViewModel>>> List(StoreParamsFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreParamsViewModel>> FindById(string id);

        Task<BaseReturn<StoreParamsViewModel>> Insert(StoreParamsViewModel viewModel);

        Task<BaseReturn<StoreParamsViewModel>> Update(StoreParamsViewModel viewModel, string id);

        Task<BaseReturn<StoreParamsViewModel>> Delete(string id);

        Task<BaseReturn<List<StoreParamsViewModel>>> Download(StoreParamsFilter filter);

    }
}
