using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IStoreImageApplication
    {
        Task<BasePaginationReturn<List<StoreImageViewModel>>> List(StoreImageFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreImageViewModel>> FindById(string id);

        Task<BaseReturn<StoreImageViewModel>> Insert(StoreImageViewModel viewModel);

        Task<BaseReturn<StoreImageViewModel>> Update(StoreImageViewModel viewModel, string id);

        Task<BaseReturn<StoreImageViewModel>> Delete(string id);

        Task<BaseReturn<List<StoreImageViewModel>>> Download(StoreImageFilter filter);

    }
}
