using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IStoreServiceApplication
    {
        Task<BasePaginationReturn<List<StoreServiceViewModel>>> List(StoreServiceFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreServiceViewModel>> FindById(string id);

        Task<BaseReturn<StoreServiceViewModel>> Insert(StoreServiceViewModel viewModel);

        Task<BaseReturn<StoreServiceViewModel>> Update(StoreServiceViewModel viewModel, string id);

        Task<BaseReturn<StoreServiceViewModel>> Delete(string id);

        Task<BaseReturn<List<StoreServiceViewModel>>> Download(StoreServiceFilter filter);

    }
}
