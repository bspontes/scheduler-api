using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IPhoneApplication
    {
        Task<BasePaginationReturn<List<PhoneViewModel>>> List(PhoneFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<PhoneViewModel>> FindById(string id);

        Task<BaseReturn<PhoneViewModel>> Insert(PhoneViewModel viewModel);

        Task<BaseReturn<PhoneViewModel>> Update(PhoneViewModel viewModel, string id);

        Task<BaseReturn<PhoneViewModel>> Delete(string id);

        Task<BaseReturn<List<PhoneViewModel>>> Download(PhoneFilter filter);

    }
}
