using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IStoreApplication
    {
        Task<BasePaginationReturn<List<StoreViewModel>>> List(StoreFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreViewModel>> FindById(string id);

        Task<BaseReturn<StoreViewModel>> Insert(StoreViewModel viewModel);

        Task<BaseReturn<StoreViewModel>> Update(StoreViewModel viewModel, string id);

        Task<BaseReturn<StoreViewModel>> Delete(string id);

        Task<BaseReturn<List<StoreViewModel>>> Download(StoreFilter filter);

    }
}
