using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IStoreBusinessTimeApplication
    {
        Task<BasePaginationReturn<List<StoreBusinessTimeViewModel>>> List(StoreBusinessTimeFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreBusinessTimeViewModel>> FindById(string id);

        Task<BaseReturn<StoreBusinessTimeViewModel>> Insert(StoreBusinessTimeViewModel viewModel);

        Task<BaseReturn<StoreBusinessTimeViewModel>> Update(StoreBusinessTimeViewModel viewModel, string id);

        Task<BaseReturn<StoreBusinessTimeViewModel>> Delete(string id);

        Task<BaseReturn<List<StoreBusinessTimeViewModel>>> Download(StoreBusinessTimeFilter filter);

    }
}
