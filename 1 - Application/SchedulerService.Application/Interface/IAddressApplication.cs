using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IAddressApplication
    {
        Task<BasePaginationReturn<List<AddressViewModel>>> List(AddressFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<AddressViewModel>> FindById(string id);

        Task<BaseReturn<AddressViewModel>> Insert(AddressViewModel viewModel);

        Task<BaseReturn<AddressViewModel>> Update(AddressViewModel viewModel, string id);

        Task<BaseReturn<AddressViewModel>> Delete(string id);

        Task<BaseReturn<List<AddressViewModel>>> Download(AddressFilter filter);

    }
}
