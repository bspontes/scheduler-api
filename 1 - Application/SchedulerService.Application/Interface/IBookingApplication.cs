using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IBookingApplication
    {
        Task<BasePaginationReturn<List<BookingViewModel>>> List(BookingFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<BookingViewModel>> FindById(string id);

        Task<BaseReturn<BookingViewModel>> Insert(BookingViewModel viewModel);

        Task<BaseReturn<BookingViewModel>> Update(BookingViewModel viewModel, string id);

        Task<BaseReturn<BookingViewModel>> Delete(string id);

        Task<BaseReturn<List<BookingViewModel>>> Download(BookingFilter filter);

    }
}
