using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IUserApplication
    {
        Task<BasePaginationReturn<List<UserViewModel>>> List(UserFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<UserViewModel>> FindById(string id);

        Task<BaseReturn<UserViewModel>> Insert(UserViewModel viewModel);

        Task<BaseReturn<UserViewModel>> Update(UserViewModel viewModel, string id);

        Task<BaseReturn<UserViewModel>> Delete(string id);

        Task<BaseReturn<List<UserViewModel>>> Download(UserFilter filter);

    }
}
