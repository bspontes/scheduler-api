using SchedulerService.Domain.Common;
using SchedulerService.Domain.ViewModel;
using System.Threading.Tasks;

namespace SchedulerService.Application.Interface
{
    public interface IBackofficeAuthApplication
    {
        Task<BaseReturn<UserViewModel>> Login(BackofficeLoginViewModel viewModel);

    }
}
