﻿using SchedulerService.Domain.Enum;
using Newtonsoft.Json;
using System;

namespace SchedulerService.EventBus.Events
{
    public class IntegrationEvent
    {
        public IntegrationEvent()
        {
            Id = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
        }

        [JsonConstructor]
        public IntegrationEvent(Guid id, DateTime createDate)
        {
            Id = id;
            CreationDate = createDate;
        }

        [JsonProperty]
        public Guid Id { get; protected set; }

        [JsonProperty]
        public DateTime CreationDate { get; protected set; }

        protected string AdjustEventName(string eventName)
        {
            eventName = !eventName.Contains("IntegrationEvent") ? $"{eventName}IntegrationEvent" : eventName;

            return eventName;
        }
    }
}
