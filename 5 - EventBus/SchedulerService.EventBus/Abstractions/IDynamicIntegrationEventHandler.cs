﻿using System.Threading.Tasks;

namespace SchedulerService.EventBus.Abstractions
{
    public interface IDynamicIntegrationEventHandler
    {
        Task Handle(dynamic eventData);
    }
}
