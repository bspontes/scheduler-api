using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class BookingFilter
    {
        public string Id { get; set; }

        public string UserId { get; set; }

        public string StoreUserId { get; set; }

        public string StoreId { get; set; }

        public string StoreServiceId { get; set; }

        public string Day { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public BookingStatusEnum? Status { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(UserId) ? $"userId={UserId}," : string.Empty;
            to += !string.IsNullOrEmpty(StoreUserId) ? $"storeUserId={StoreUserId}," : string.Empty;
            to += !string.IsNullOrEmpty(StoreId) ? $"storeId={StoreId}," : string.Empty;
            to += !string.IsNullOrEmpty(StoreServiceId) ? $"storeServiceId={StoreServiceId}," : string.Empty;
            to += !string.IsNullOrEmpty(Day) ? $"day={Day}," : string.Empty;
            to += !string.IsNullOrEmpty(StartTime) ? $"startTime={StartTime}," : string.Empty;
            to += !string.IsNullOrEmpty(EndTime) ? $"endTime={EndTime}," : string.Empty;
            to += Status.HasValue ? $"status={Status}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
