using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class StoreParamsFilter
    {
        public string Id { get; set; }

        public string Description { get; set; }

        public string LogoUrl { get; set; }

        public string BackgroundUrl { get; set; }

        public string BaseColor { get; set; }

        public string TextColor { get; set; }

        public string StoreId { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(Description) ? $"description={Description}," : string.Empty;
            to += !string.IsNullOrEmpty(LogoUrl) ? $"logoUrl={LogoUrl}," : string.Empty;
            to += !string.IsNullOrEmpty(BackgroundUrl) ? $"backgroundUrl={BackgroundUrl}," : string.Empty;
            to += !string.IsNullOrEmpty(BaseColor) ? $"baseColor={BaseColor}," : string.Empty;
            to += !string.IsNullOrEmpty(TextColor) ? $"textColor={TextColor}," : string.Empty;
            to += !string.IsNullOrEmpty(StoreId) ? $"storeId={StoreId}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
