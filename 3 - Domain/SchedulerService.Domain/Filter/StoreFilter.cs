using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class StoreFilter
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string FantasyName { get; set; }

        public string Email { get; set; }

        public string Document { get; set; }

        public StoreBusinessLineEnum? BusinessLine { get; set; }

        public StoreStatusEnum? Status { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(Name) ? $"name={Name}," : string.Empty;
            to += !string.IsNullOrEmpty(FantasyName) ? $"fantasyName={FantasyName}," : string.Empty;
            to += !string.IsNullOrEmpty(Email) ? $"email={Email}," : string.Empty;
            to += !string.IsNullOrEmpty(Document) ? $"document={Document}," : string.Empty;
            to += BusinessLine.HasValue ? $"businessLine={BusinessLine}," : string.Empty;
            to += Status.HasValue ? $"status={Status}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
