using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class UserFilter
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string Document { get; set; }

        public UserSexEnum? Sex { get; set; }

        public string Profession { get; set; }

        public UserStatusEnum? Status { get; set; }

        public string ProfileUrl { get; set; }

        public UserTypeEnum? Type { get; set; }

        public UserOriginEnum? Origin { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(Name) ? $"name={Name}," : string.Empty;
            to += !string.IsNullOrEmpty(Password) ? $"password={Password}," : string.Empty;
            to += !string.IsNullOrEmpty(Email) ? $"email={Email}," : string.Empty;
            to += !string.IsNullOrEmpty(Document) ? $"document={Document}," : string.Empty;
            to += Sex.HasValue ? $"sex={Sex}," : string.Empty;
            to += !string.IsNullOrEmpty(Profession) ? $"profession={Profession}," : string.Empty;
            to += Status.HasValue ? $"status={Status}," : string.Empty;
            to += !string.IsNullOrEmpty(ProfileUrl) ? $"profileUrl={ProfileUrl}," : string.Empty;
            to += Type.HasValue ? $"type={Type}," : string.Empty;
            to += Origin.HasValue ? $"origin={Origin}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
