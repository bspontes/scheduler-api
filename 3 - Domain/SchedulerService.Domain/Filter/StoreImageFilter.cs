using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class StoreImageFilter
    {
        public string Id { get; set; }

        public string OriginalUrl { get; set; }

        public string ThumbnailUrl { get; set; }

        public string StoreId { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(OriginalUrl) ? $"originalUrl={OriginalUrl}," : string.Empty;
            to += !string.IsNullOrEmpty(ThumbnailUrl) ? $"thumbnailUrl={ThumbnailUrl}," : string.Empty;
            to += !string.IsNullOrEmpty(StoreId) ? $"storeId={StoreId}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
