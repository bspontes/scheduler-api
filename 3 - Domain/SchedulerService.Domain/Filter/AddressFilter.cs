using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class AddressFilter
    {
        public string Id { get; set; }

        public string Zipcode { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }

        public string Complement { get; set; }

        public string Neighborhood { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Zipcode) ? $"zipcode={Zipcode}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Street) ? $"street={Street}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Number) ? $"number={Number}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Complement) ? $"complement={Complement}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Neighborhood) ? $"neighborhood={Neighborhood}," : string.Empty; ;
            to += !string.IsNullOrEmpty(City) ? $"city={City}," : string.Empty; ;
            to += !string.IsNullOrEmpty(State) ? $"state={State}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Country) ? $"country={Country}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Latitude) ? $"latitude={Latitude}," : string.Empty; ;
            to += !string.IsNullOrEmpty(Longitude) ? $"longitude={Longitude}," : string.Empty; ;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
