using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class StoreServiceFilter
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Duration { get; set; }

        public string Price { get; set; }

        public StoreServicePriceTypeEnum? PriceType { get; set; }

        public string StoreId { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(Name) ? $"name={Name}," : string.Empty;
            to += !string.IsNullOrEmpty(Type) ? $"type={Type}," : string.Empty;
            to += !string.IsNullOrEmpty(Duration) ? $"duration={Duration}," : string.Empty;
            to += !string.IsNullOrEmpty(Price) ? $"price={Price}," : string.Empty;
            to += PriceType.HasValue ? $"priceType={PriceType}," : string.Empty;
            to += !string.IsNullOrEmpty(StoreId) ? $"storeId={StoreId}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
