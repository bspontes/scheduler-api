using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class StoreBusinessTimeFilter
    {
        public string Id { get; set; }

        public string DayOfWeek { get; set; }

        public string StartTime { get; set; }

        public string LunchTime { get; set; }

        public string EndTime { get; set; }

        public string StoreId { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(DayOfWeek) ? $"dayOfWeek={DayOfWeek}," : string.Empty;
            to += !string.IsNullOrEmpty(StartTime) ? $"startTime={StartTime}," : string.Empty;
            to += !string.IsNullOrEmpty(LunchTime) ? $"lunchTime={LunchTime}," : string.Empty;
            to += !string.IsNullOrEmpty(EndTime) ? $"endTime={EndTime}," : string.Empty;
            to += !string.IsNullOrEmpty(StoreId) ? $"storeId={StoreId}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
