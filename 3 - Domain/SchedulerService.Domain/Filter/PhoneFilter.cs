using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Filter
{
    public class PhoneFilter
    {
        public string Id { get; set; }

        public string CountryCode { get; set; }

        public string AreaCode { get; set; }

        public string Number { get; set; }

        public DateTime? CreatedAtStart { get; set; }

        public DateTime? CreatedAtEnd { get; set; }

        public DateTime? UpdatedAtStart { get; set; }

        public DateTime? UpdatedAtEnd { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Id) ? $"id={Id}," : string.Empty;
            to += !string.IsNullOrEmpty(CountryCode) ? $"countryCode={CountryCode}," : string.Empty;
            to += !string.IsNullOrEmpty(AreaCode) ? $"areaCode={AreaCode}," : string.Empty;
            to += !string.IsNullOrEmpty(Number) ? $"number={Number}," : string.Empty;
            to += CreatedAtStart.HasValue ? $"createdAtStart={CreatedAtStart.Value}," : string.Empty;
            to += CreatedAtEnd.HasValue ? $"createdAtEnd={CreatedAtEnd.Value}," : string.Empty;
            to += UpdatedAtStart.HasValue ? $"updatedAtStart={UpdatedAtStart.Value}," : string.Empty;
            to += UpdatedAtEnd.HasValue ? $"updatedAtEnd={UpdatedAtEnd.Value}," : string.Empty;

            return to;
        }
    }
}
