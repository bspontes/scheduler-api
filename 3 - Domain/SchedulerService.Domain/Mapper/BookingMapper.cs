using SchedulerService.Domain.Commands.Booking;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class BookingMapper
    {
        public Expression<Func<Booking, bool>> Filter(BookingFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<Booking, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.UserId) ? x.UserId.Contains(filter.UserId) : x != null &&
            !string.IsNullOrEmpty(filter.StoreUserId) ? x.StoreUserId.Contains(filter.StoreUserId) : x != null &&
            !string.IsNullOrEmpty(filter.StoreId) ? x.StoreId.Contains(filter.StoreId) : x != null &&
            !string.IsNullOrEmpty(filter.StoreServiceId) ? x.StoreServiceId.Contains(filter.StoreServiceId) : x != null &&
            !string.IsNullOrEmpty(filter.Day) ? x.Day.Contains(filter.Day) : x != null &&
            !string.IsNullOrEmpty(filter.StartTime) ? x.StartTime.Contains(filter.StartTime) : x != null &&
            !string.IsNullOrEmpty(filter.EndTime) ? x.EndTime.Contains(filter.EndTime) : x != null &&
            filter.Status.HasValue ? x.Status == filter.Status : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public Booking Update(Booking model, BookingCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.UserId = command.UserId;
            model.StoreUserId = command.StoreUserId;
            model.StoreId = command.StoreId;
            model.StoreServiceId = command.StoreServiceId;
            model.Day = command.Day;
            model.StartTime = command.StartTime;
            model.EndTime = command.EndTime;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public Booking Model(BookingCommand command)
        {
            if (command == null) return null;

            return new Booking
            {
                Id = Guid.NewGuid().ToString(),
                Day = command.Day,
                StartTime = command.StartTime,
                EndTime = command.EndTime,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public BookingViewModel ViewModel(Booking model)
        {
            if (model == null) return null;

            return new BookingViewModel
            {
                Id = model.Id,
                UserId = model.UserId,
                StoreUserId = model.StoreUserId,
                StoreId = model.StoreId,
                StoreServiceId = model.StoreServiceId,
                Day = model.Day,
                StartTime = model.StartTime,
                EndTime = model.EndTime,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<Booking> Model(List<BookingCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<BookingViewModel> ViewModel(List<Booking> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<BookingViewModel>> ViewModel(BasePagination<List<Booking>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<BookingViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
