using SchedulerService.Domain.Commands.Store;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class StoreMapper
    {
        public Expression<Func<Store, bool>> Filter(StoreFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<Store, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.Name) ? x.Name.Contains(filter.Name) : x != null &&
            !string.IsNullOrEmpty(filter.FantasyName) ? x.FantasyName.Contains(filter.FantasyName) : x != null &&
            !string.IsNullOrEmpty(filter.Email) ? x.Email.Contains(filter.Email) : x != null &&
            !string.IsNullOrEmpty(filter.Document) ? x.Document.Contains(filter.Document) : x != null &&
            filter.BusinessLine.HasValue ? x.BusinessLine == filter.BusinessLine : x != null &&
            filter.Status.HasValue ? x.Status == filter.Status : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public Store Update(Store model, StoreCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.Name = command.Name;
            model.FantasyName = command.FantasyName;
            model.Email = command.Email;
            model.Document = command.Document;
            model.BusinessLine = command.BusinessLine.Value;
            model.Status = command.Status.Value;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public Store Model(StoreCommand command)
        {
            if (command == null) return null;

            return new Store
            {
                Id = Guid.NewGuid().ToString(),
                Name = command.Name,
                FantasyName = command.FantasyName,
                Email = command.Email,
                Document = command.Document,
                BusinessLine = command.BusinessLine.Value,
                Status = command.Status.Value,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public StoreViewModel ViewModel(Store model)
        {
            if (model == null) return null;

            return new StoreViewModel
            {
                Id = model.Id,
                Name = model.Name,
                FantasyName = model.FantasyName,
                Email = model.Email,
                Document = model.Document,
                BusinessLine = model.BusinessLine,
                Status = model.Status,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<Store> Model(List<StoreCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<StoreViewModel> ViewModel(List<Store> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<StoreViewModel>> ViewModel(BasePagination<List<Store>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<StoreViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
