using SchedulerService.Domain.Commands.Phone;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class PhoneMapper
    {
        public Expression<Func<Phone, bool>> Filter(PhoneFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<Phone, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.CountryCode) ? x.CountryCode.Contains(filter.CountryCode) : x != null &&
            !string.IsNullOrEmpty(filter.AreaCode) ? x.AreaCode.Contains(filter.AreaCode) : x != null &&
            !string.IsNullOrEmpty(filter.Number) ? x.Number.Contains(filter.Number) : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public Phone Update(Phone model, PhoneCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.CountryCode = command.CountryCode;
            model.AreaCode = command.AreaCode;
            model.Number = command.Number;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public Phone Model(PhoneCommand command)
        {
            if (command == null) return null;

            return new Phone
            {
                Id = Guid.NewGuid().ToString(),
                CountryCode = command.CountryCode,
                AreaCode = command.AreaCode,
                Number = command.Number,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public PhoneViewModel ViewModel(Phone model)
        {
            if (model == null) return null;

            return new PhoneViewModel
            {
                Id = model.Id,
                CountryCode = model.CountryCode,
                AreaCode = model.AreaCode,
                Number = model.Number,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<Phone> Model(List<PhoneCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<PhoneViewModel> ViewModel(List<Phone> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<PhoneViewModel>> ViewModel(BasePagination<List<Phone>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<PhoneViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
