using SchedulerService.Domain.Commands.Address;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class AddressMapper
    {
        public Expression<Func<Address, bool>> Filter(AddressFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<Address, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.Zipcode) ? x.Zipcode.Contains(filter.Zipcode) : x != null &&
            !string.IsNullOrEmpty(filter.Street) ? x.Street.Contains(filter.Street) : x != null &&
            !string.IsNullOrEmpty(filter.Number) ? x.Number.Contains(filter.Number) : x != null &&
            !string.IsNullOrEmpty(filter.Complement) ? x.Complement.Contains(filter.Complement) : x != null &&
            !string.IsNullOrEmpty(filter.Neighborhood) ? x.Neighborhood.Contains(filter.Neighborhood) : x != null &&
            !string.IsNullOrEmpty(filter.City) ? x.City.Contains(filter.City) : x != null &&
            !string.IsNullOrEmpty(filter.State) ? x.State.Contains(filter.State) : x != null &&
            !string.IsNullOrEmpty(filter.Country) ? x.Country.Contains(filter.Country) : x != null &&
            !string.IsNullOrEmpty(filter.Latitude) ? x.Latitude.Contains(filter.Latitude) : x != null &&
            !string.IsNullOrEmpty(filter.Longitude) ? x.Longitude.Contains(filter.Longitude) : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public Address Update(Address model, AddressCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.Zipcode = command.Zipcode;
            model.Street = command.Street;
            model.Number = command.Number;
            model.Complement = command.Complement;
            model.Neighborhood = command.Neighborhood;
            model.City = command.City;
            model.State = command.State;
            model.Country = command.Country;
            model.Latitude = command.Latitude;
            model.Longitude = command.Longitude;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public Address Model(AddressCommand command)
        {
            if (command == null) return null;

            return new Address
            {
                Id = Guid.NewGuid().ToString(),
                Zipcode = command.Zipcode,
                Street = command.Street,
                Number = command.Number,
                Complement = command.Complement,
                Neighborhood = command.Neighborhood,
                City = command.City,
                State = command.State,
                Country = command.Country,
                Latitude = command.Latitude,
                Longitude = command.Longitude,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public AddressViewModel ViewModel(Address model)
        {
            if (model == null) return null;

            return new AddressViewModel
            {
                Id = model.Id,
                Zipcode = model.Zipcode,
                Street = model.Street,
                Number = model.Number,
                Complement = model.Complement,
                Neighborhood = model.Neighborhood,
                City = model.City,
                State = model.State,
                Country = model.Country,
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<Address> Model(List<AddressCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<AddressViewModel> ViewModel(List<Address> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<AddressViewModel>> ViewModel(BasePagination<List<Address>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<AddressViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
