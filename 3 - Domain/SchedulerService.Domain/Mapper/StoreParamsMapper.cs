using SchedulerService.Domain.Commands.StoreParams;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class StoreParamsMapper
    {
        public Expression<Func<StoreParams, bool>> Filter(StoreParamsFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<StoreParams, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.Description) ? x.Id.Contains(filter.Description) : x != null &&
            !string.IsNullOrEmpty(filter.LogoUrl) ? x.Id.Contains(filter.LogoUrl) : x != null &&
            !string.IsNullOrEmpty(filter.BackgroundUrl) ? x.Id.Contains(filter.BackgroundUrl) : x != null &&
            !string.IsNullOrEmpty(filter.BaseColor) ? x.Id.Contains(filter.BaseColor) : x != null &&
            !string.IsNullOrEmpty(filter.TextColor) ? x.Id.Contains(filter.TextColor) : x != null &&
            !string.IsNullOrEmpty(filter.StoreId) ? x.Id.Contains(filter.StoreId) : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public StoreParams Update(StoreParams model, StoreParamsCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.Description = command.Description;
            model.LogoUrl = command.LogoUrl;
            model.BackgroundUrl = command.BackgroundUrl;
            model.BaseColor = command.BaseColor;
            model.TextColor = command.TextColor;
            model.StoreId = command.StoreId;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public StoreParams Model(StoreParamsCommand command)
        {
            if (command == null) return null;

            return new StoreParams
            {
                Id = Guid.NewGuid().ToString(),
                Description = command.Description,
                LogoUrl = command.LogoUrl,
                BackgroundUrl = command.BackgroundUrl,
                BaseColor = command.BaseColor,
                TextColor = command.TextColor,

                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public StoreParamsViewModel ViewModel(StoreParams model)
        {
            if (model == null) return null;

            return new StoreParamsViewModel
            {
                Id = model.Id,
                Description = model.Description,
                LogoUrl = model.LogoUrl,
                BackgroundUrl = model.BackgroundUrl,
                BaseColor = model.BaseColor,
                TextColor = model.TextColor,
                StoreId = model.StoreId,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<StoreParams> Model(List<StoreParamsCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<StoreParamsViewModel> ViewModel(List<StoreParams> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<StoreParamsViewModel>> ViewModel(BasePagination<List<StoreParams>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<StoreParamsViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
