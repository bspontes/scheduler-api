using SchedulerService.Domain.Commands.StoreService;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class StoreServiceMapper
    {
        public Expression<Func<StoreService, bool>> Filter(StoreServiceFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<StoreService, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.Name) ? x.Id.Contains(filter.Name) : x != null &&
            !string.IsNullOrEmpty(filter.Type) ? x.Id.Contains(filter.Type) : x != null &&
            !string.IsNullOrEmpty(filter.Duration) ? x.Id.Contains(filter.Duration) : x != null &&
            !string.IsNullOrEmpty(filter.Price) ? x.Id.Contains(filter.Price) : x != null &&
            filter.PriceType.HasValue ? x.PriceType == filter.PriceType : x != null &&
            !string.IsNullOrEmpty(filter.StoreId) ? x.Id.Contains(filter.StoreId) : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public StoreService Update(StoreService model, StoreServiceCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.Name = command.Name;
            model.Type = command.StoreServiceType;
            model.Duration = command.Duration;
            model.Price = command.Price;
            model.PriceType = command.PriceType.Value;
            model.StoreId = command.StoreId;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public StoreService Model(StoreServiceCommand command)
        {
            if (command == null) return null;

            return new StoreService
            {
                Id = Guid.NewGuid().ToString(),
                Name = command.Name,
                Type = command.StoreServiceType,
                Duration = command.Duration,
                Price = command.Price,
                PriceType = command.PriceType.Value,

                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public StoreServiceViewModel ViewModel(StoreService model)
        {
            if (model == null) return null;

            return new StoreServiceViewModel
            {
                Id = model.Id,
                Name = model.Name,
                Type = model.Type,
                Duration = model.Duration,
                Price = model.Price,
                PriceType = model.PriceType,
                StoreId = model.StoreId,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<StoreService> Model(List<StoreServiceCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<StoreServiceViewModel> ViewModel(List<StoreService> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<StoreServiceViewModel>> ViewModel(BasePagination<List<StoreService>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<StoreServiceViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
