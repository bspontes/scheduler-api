using SchedulerService.Domain.Commands.StoreBusinessTime;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class StoreBusinessTimeMapper
    {
        public Expression<Func<StoreBusinessTime, bool>> Filter(StoreBusinessTimeFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<StoreBusinessTime, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.DayOfWeek) ? x.DayOfWeek.Contains(filter.DayOfWeek) : x != null &&
            !string.IsNullOrEmpty(filter.StartTime) ? x.StartTime.Contains(filter.StartTime) : x != null &&
            !string.IsNullOrEmpty(filter.LunchTime) ? x.LunchTime.Contains(filter.LunchTime) : x != null &&
            !string.IsNullOrEmpty(filter.EndTime) ? x.EndTime.Contains(filter.EndTime) : x != null &&
            !string.IsNullOrEmpty(filter.StoreId) ? x.StoreId.Contains(filter.StoreId) : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public StoreBusinessTime Update(StoreBusinessTime model, StoreBusinessTimeCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.DayOfWeek = command.DayOfWeek;
            model.StartTime = command.StartTime;
            model.LunchTime = command.LunchTime;
            model.EndTime = command.EndTime;
            model.StoreId = command.StoreId;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public StoreBusinessTime Model(StoreBusinessTimeCommand command)
        {
            if (command == null) return null;

            return new StoreBusinessTime
            {
                Id = Guid.NewGuid().ToString(),
                DayOfWeek = command.DayOfWeek,
                StartTime = command.StartTime,
                LunchTime = command.LunchTime,
                EndTime = command.EndTime,

                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public StoreBusinessTimeViewModel ViewModel(StoreBusinessTime model)
        {
            if (model == null) return null;

            return new StoreBusinessTimeViewModel
            {
                Id = model.Id,
                DayOfWeek = model.DayOfWeek,
                StartTime = model.StartTime,
                LunchTime = model.LunchTime,
                EndTime = model.EndTime,
                StoreId = model.StoreId,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<StoreBusinessTime> Model(List<StoreBusinessTimeCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<StoreBusinessTimeViewModel> ViewModel(List<StoreBusinessTime> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<StoreBusinessTimeViewModel>> ViewModel(BasePagination<List<StoreBusinessTime>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<StoreBusinessTimeViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
