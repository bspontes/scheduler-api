using SchedulerService.Domain.Commands.StoreImage;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class StoreImageMapper
    {
        public Expression<Func<StoreImage, bool>> Filter(StoreImageFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<StoreImage, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.OriginalUrl) ? x.OriginalUrl.Contains(filter.OriginalUrl) : x != null &&
            !string.IsNullOrEmpty(filter.ThumbnailUrl) ? x.ThumbnailUrl.Contains(filter.ThumbnailUrl) : x != null &&
            !string.IsNullOrEmpty(filter.StoreId) ? x.StoreId.Contains(filter.StoreId) : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public StoreImage Update(StoreImage model, StoreImageCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.OriginalUrl = command.OriginalUrl;
            model.ThumbnailUrl = command.ThumbnailUrl;
            model.StoreId = command.StoreId;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public StoreImage Model(StoreImageCommand command)
        {
            if (command == null) return null;

            return new StoreImage
            {
                Id = Guid.NewGuid().ToString(),
                OriginalUrl = command.OriginalUrl,
                ThumbnailUrl = command.ThumbnailUrl,

                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public StoreImageViewModel ViewModel(StoreImage model)
        {
            if (model == null) return null;

            return new StoreImageViewModel
            {
                Id = model.Id,
                OriginalUrl = model.OriginalUrl,
                ThumbnailUrl = model.ThumbnailUrl,
                StoreId = model.StoreId,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<StoreImage> Model(List<StoreImageCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<StoreImageViewModel> ViewModel(List<StoreImage> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<StoreImageViewModel>> ViewModel(BasePagination<List<StoreImage>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<StoreImageViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
