using SchedulerService.Domain.Commands.User;
using SchedulerService.Domain.Core.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using SchedulerService.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SchedulerService.Domain.Mapper
{
    public class UserMapper
    {
        public Expression<Func<User, bool>> Filter(UserFilter filter)
        {
            if (filter == null) return null;

            Expression<Func<User, bool>> expression = x =>
            !string.IsNullOrEmpty(filter.Id) ? x.Id.Contains(filter.Id) : x != null &&
            !string.IsNullOrEmpty(filter.Name) ? x.Name.Contains(filter.Name) : x != null &&
            !string.IsNullOrEmpty(filter.Password) ? x.Password.Contains(filter.Password) : x != null &&
            !string.IsNullOrEmpty(filter.Email) ? x.Email.Contains(filter.Email) : x != null &&
            !string.IsNullOrEmpty(filter.Document) ? x.Document.Contains(filter.Document) : x != null &&
            filter.Sex.HasValue ? x.Sex == filter.Sex : x != null &&
            !string.IsNullOrEmpty(filter.Profession) ? x.Profession.Contains(filter.Profession) : x != null &&
            filter.Status.HasValue ? x.Status == filter.Status : x != null &&
            !string.IsNullOrEmpty(filter.ProfileUrl) ? x.ProfileUrl.Contains(filter.ProfileUrl) : x != null &&
            filter.Type.HasValue ? x.Type == filter.Type : x != null &&
            filter.Origin.HasValue ? x.Origin == filter.Origin : x != null &&
            filter.CreatedAtStart.HasValue && filter.CreatedAtEnd.HasValue ? x.CreatedAt >= filter.CreatedAtStart.Value && x.CreatedAt <= filter.CreatedAtEnd.Value : x != null &&
            filter.UpdatedAtStart.HasValue && filter.UpdatedAtEnd.HasValue ? x.CreatedAt >= filter.UpdatedAtStart.Value && x.CreatedAt <= filter.UpdatedAtEnd.Value : x != null;

            return expression;
        }

        public User Update(User model, UserCommand command)
        {
            if (model == null || command == null) return null;

            model.Id = command.Id;
            model.Name = command.Name;
            model.Password = command.Password;
            model.Email = command.Email;
            model.Document = command.Document;
            model.Sex = command.Sex.Value;
            model.Profession = command.Profession;
            model.Status = command.Status.Value;
            model.ProfileUrl = command.ProfileUrl;
            model.Type = command.UserType.Value;
            model.Origin = command.Origin.Value;
            model.UpdatedAt = DateTime.Now;

            return model;
        }

        public User Model(UserCommand command)
        {
            if (command == null) return null;

            return new User
            {
                Id = Guid.NewGuid().ToString(),
                Name = command.Name,
                Password = command.Password,
                Email = command.Email,
                Document = command.Document,
                Sex = command.Sex.Value,
                Profession = command.Profession,
                Status = command.Status.Value,
                ProfileUrl = command.ProfileUrl,
                Type = command.UserType.Value,
                Origin = command.Origin.Value,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
        }

        public UserViewModel ViewModel(User model)
        {
            if (model == null) return null;

            return new UserViewModel
            {
                Id = model.Id,
                Name = model.Name,
                Password = model.Password,
                Email = model.Email,
                Document = model.Document,
                Sex = model.Sex,
                Profession = model.Profession,
                Status = model.Status,
                ProfileUrl = model.ProfileUrl,
                Type = model.Type,
                Origin = model.Origin,
                CreatedAt = model.CreatedAt,
                UpdatedAt = model.UpdatedAt,
            };
        }

        public List<User> Model(List<UserCommand> commands)
        {
            return commands?.Select(Model).ToList();
        }

        public List<UserViewModel> ViewModel(List<User> models)
        {
            return models?.Select(ViewModel).ToList();
        }

        public BasePagination<List<UserViewModel>> ViewModel(BasePagination<List<User>> model)
        {
            if (model == null) return null;

            return new BasePagination<List<UserViewModel>>
            {
                Total = model.Total,
                LastUpdate = model.LastUpdate,
                Data = model.Data.Select(ViewModel).ToList()
            };
        }
    }
}
