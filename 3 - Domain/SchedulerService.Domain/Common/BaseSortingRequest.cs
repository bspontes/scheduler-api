﻿using SchedulerService.Domain.Enum;
using Microsoft.AspNetCore.Mvc;

namespace SchedulerService.Domain.Common
{
    public class BaseSortingRequest
    {
        public BaseSortingRequest()
        {
            Field = "CreatedAt";
            TypeSorting = TypeSortingEnum.DESC;
        }

        [FromQuery(Name = "field")]
        public string Field { get; set; }

        [FromQuery(Name = "sort")]
        public TypeSortingEnum TypeSorting { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += !string.IsNullOrEmpty(Field) ? $"field={Field}," : string.Empty;
            to += $"typeSorting={TypeSorting},";

            return to;
        }
    }
}
