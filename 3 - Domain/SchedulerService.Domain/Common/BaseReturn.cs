﻿namespace SchedulerService.Domain.Common
{
    public class BaseReturn<T>
    {
        public BaseReturn()
        {
            Success = true;
            Description = string.Empty;
            Data = default;
        }

        public bool Success { get; set; }

        public string Description { get; set; }

        public T Data { get; set; }
    }
}
