﻿using SchedulerService.Domain.Core.Common;
using System;

namespace SchedulerService.Domain.Common
{
    public class BasePaginationReturn<T>
    {
        public BasePaginationReturn()
        {
            Success = true;
            Description = string.Empty;
            Data = default;
        }

        public bool Success { get; set; }

        public string Description { get; set; }

        public BasePagination<T> Data { get; set; }
    }
}
