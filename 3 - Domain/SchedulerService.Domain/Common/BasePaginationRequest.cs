﻿using Microsoft.AspNetCore.Mvc;

namespace SchedulerService.Domain.Common
{
    public class BasePaginationRequest
    {
        public BasePaginationRequest()
        {
            Page = 0;
            PageSize = 20;
        }

        [FromQuery(Name = "page")]
        public int Page { get; set; }

        [FromQuery(Name = "pageSize")]
        public int PageSize { get; set; }

        public override string ToString()
        {
            var to = string.Empty;

            to += $"page={Page},";
            to += $"pageSize={PageSize}";

            return to;
        }
    }
}
