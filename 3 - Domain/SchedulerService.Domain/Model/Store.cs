using SchedulerService.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("Store")]
    public class Store
    {

        [MaxLength(40)]
        public string Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(100)]
        public string FantasyName { get; set; }

        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        [MaxLength(20)]
        public string Document { get; set; }

        [Required]
        [MaxLength(100)]
        public StoreBusinessLineEnum BusinessLine { get; set; }

        [Required]
        [MaxLength(100)]
        public StoreStatusEnum Status { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual StoreParams Params { get; set; }

        public virtual Booking Booking { get; set; }

        public virtual List<StoreBusinessTime> BusinessTimes { get; set; }

        public virtual List<StoreService> Services { get; set; }

        public virtual List<StoreImage> Images { get; set; }

        public virtual List<StorePhone> StorePhones { get; set; }

        public virtual List<StoreAddress> StoreAddresses { get; set; }


    }
}
