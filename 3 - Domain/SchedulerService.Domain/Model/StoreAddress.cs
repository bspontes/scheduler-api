using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("StoreAddress")]
    public class StoreAddress
    {
        public virtual string StoreId { get; set; }

        public virtual string AddressId { get; set; }

        public virtual Store Store { get; set; }

        public virtual Address Address { get; set; }

    }
}