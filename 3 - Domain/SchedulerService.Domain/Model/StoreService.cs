using SchedulerService.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("StoreService")]
    public class StoreService
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Type { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Duration { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Price { get; set; }
        
        [Required]
        [MaxLength(100)]
        public StoreServicePriceTypeEnum PriceType { get; set; }
        
        [MaxLength(40)]
        public string StoreId { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual Store Store { get; set; }

        public virtual Booking Booking { get; set; }

    }
}
