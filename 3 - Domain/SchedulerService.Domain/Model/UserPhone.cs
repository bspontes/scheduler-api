using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("UserPhone")]
    public class UserPhone
    {
        public virtual string UserId { get; set; }

        public virtual string PhoneId { get; set; }

        public virtual User User { get; set; }

        public virtual Phone Phone { get; set; }

    }
}
