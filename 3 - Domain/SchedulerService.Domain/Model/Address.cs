using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("Address")]
    public class Address
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(10)]
        public string Zipcode { get; set; }
        
        [Required]
        [MaxLength(150)]
        public string Street { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string Number { get; set; }
        
        [MaxLength(255)]
        public string Complement { get; set; }
        
        [Required]
        [MaxLength(150)]
        public string Neighborhood { get; set; }
        
        [Required]
        [MaxLength(150)]
        public string City { get; set; }
        
        [Required]
        [MaxLength(2)]
        public string State { get; set; }
        
        [Required]
        [MaxLength(5)]
        public string Country { get; set; }
        
        [Required]
        [MaxLength(30)]
        public string Latitude { get; set; }
        
        [Required]
        [MaxLength(30)]
        public string Longitude { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual List<StoreAddress> StoreAddresses { get; set; }

    }
}
