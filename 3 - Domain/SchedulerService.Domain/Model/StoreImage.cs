using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("StoreImage")]
    public class StoreImage
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string OriginalUrl { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string ThumbnailUrl { get; set; }
        
        [MaxLength(40)]
        public string StoreId { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual Store Store { get; set; }

    }
}
