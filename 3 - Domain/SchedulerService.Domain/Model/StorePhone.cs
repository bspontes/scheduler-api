using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("StorePhone")]
    public class StorePhone
    {
        public virtual string StoreId { get; set; }

        public virtual string PhoneId { get; set; }

        public virtual Store Store { get; set; }

        public virtual Phone Phone { get; set; }

    }
}