using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("Phone")]
    public class Phone
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(3)]
        public string CountryCode { get; set; }
        
        [Required]
        [MaxLength(3)]
        public string AreaCode { get; set; }
        
        [Required]
        [MaxLength(10)]
        public string Number { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual List<StorePhone> StorePhones { get; set; }

        public virtual List<UserPhone> UserPhones { get; set; }

    }
}
