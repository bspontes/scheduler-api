using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("StoreParams")]
    public class StoreParams
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string Description { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string LogoUrl { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string BackgroundUrl { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string BaseColor { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string TextColor { get; set; }
        
        [MaxLength(40)]
        public string StoreId { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual Store Store { get; set; }

    }
}
