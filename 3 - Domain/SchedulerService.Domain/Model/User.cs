using SchedulerService.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("User")]
    public class User
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        
        [Required]
        [MaxLength(20)]
        public string Password { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }
        
        [Required]
        [MaxLength(15)]
        public string Document { get; set; }
        
        [Required]
        [MaxLength(20)]
        public UserSexEnum Sex { get; set; }
        
        [MaxLength(100)]
        public string Profession { get; set; }
        
        [Required]
        [MaxLength(100)]
        public UserStatusEnum Status { get; set; }
        
        [Required]
        [MaxLength(255)]
        public string ProfileUrl { get; set; }
        
        [Required]
        [MaxLength(100)]
        public UserTypeEnum Type { get; set; }
        
        [Required]
        [MaxLength(100)]
        public UserOriginEnum Origin { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual Booking Booking { get; set; }

        public virtual List<UserPhone> UserPhones { get; set; }


    }
}
