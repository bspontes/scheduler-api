using SchedulerService.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("Booking")]
    public class Booking
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [MaxLength(40)]
        public string UserId { get; set; }
        
        [MaxLength(40)]
        public string StoreUserId { get; set; }
        
        [MaxLength(40)]
        public string StoreId { get; set; }
        
        [MaxLength(40)]
        public string StoreServiceId { get; set; }
        
        [Required]
        [MaxLength(40)]
        public string Day { get; set; }
        
        [Required]
        [MaxLength(40)]
        public string StartTime { get; set; }
        
        [Required]
        [MaxLength(40)]
        public string EndTime { get; set; }

        [Required]
        [MaxLength(100)]
        public BookingStatusEnum Status { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual User User { get; set; }

        public virtual Store Store { get; set; }

        public virtual StoreService Service { get; set; }

    }
}
