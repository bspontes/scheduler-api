using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Model
{
    [Table("StoreBusinessTime")]
    public class StoreBusinessTime
    {
        
        [MaxLength(40)]
        public string Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string DayOfWeek { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string StartTime { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string LunchTime { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string EndTime { get; set; }
        
        [MaxLength(40)]
        public string StoreId { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual Store Store { get; set; }


    }
}
