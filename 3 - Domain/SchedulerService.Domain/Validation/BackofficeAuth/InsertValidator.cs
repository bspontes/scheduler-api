
namespace SchedulerService.Domain.Validation.BackofficeAuth
{
    public class InsertValidator : BackofficeAuthValidator
    {
        public InsertValidator()
        {
            ValidateEmail();
            ValidatePassword();
            ValidateType();

        }
    }
}
