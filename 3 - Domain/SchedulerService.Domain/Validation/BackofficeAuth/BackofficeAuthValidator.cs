using FluentValidation;
using SchedulerService.Domain.Commands.BackofficeAuth;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.BackofficeAuth
{
    public class BackofficeAuthValidator : BaseValidation<BackofficeAuthCommand>
    {
        protected void ValidateEmail()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidatePassword()
        {
            RuleFor(x => x.Password)
                .NotEmpty()
                .NotNull()
                .MaximumLength(20);
        }

        protected void ValidateType()
        {
            RuleFor(x => x.UserType)
                .IsInEnum();
        }


    }
}
