namespace SchedulerService.Domain.Validation.Address
{
    public class DeleteValidator : AddressValidator
    {
        public DeleteValidator()
        {
            ValidateId();
        }
    }
}
