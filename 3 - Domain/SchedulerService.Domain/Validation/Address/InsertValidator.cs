
namespace SchedulerService.Domain.Validation.Address
{
    public class InsertValidator : AddressValidator
    {
        public InsertValidator()
        {
            ValidateZipcode();
            ValidateStreet();
            ValidateNumber();
            ValidateComplement();
            ValidateNeighborhood();
            ValidateCity();
            ValidateState();
            ValidateCountry();
            ValidateLatitude();
            ValidateLongitude();

        }
    }
}
