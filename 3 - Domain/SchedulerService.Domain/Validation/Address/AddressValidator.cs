using FluentValidation;
using SchedulerService.Domain.Commands.Address;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.Address
{
    public class AddressValidator : BaseValidation<AddressCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateZipcode()
        {
            RuleFor(x => x.Zipcode)
                .NotEmpty()
                .NotNull()
                .MaximumLength(10);
        }

        protected void ValidateStreet()
        {
            RuleFor(x => x.Street)
                .NotEmpty()
                .NotNull()
                .MaximumLength(150);
        }

        protected void ValidateNumber()
        {
            RuleFor(x => x.Number)
                .NotEmpty()
                .NotNull()
                .MaximumLength(20);
        }

        protected void ValidateComplement()
        {
            RuleFor(x => x.Complement)
                .MaximumLength(255);
        }

        protected void ValidateNeighborhood()
        {
            RuleFor(x => x.Neighborhood)
                .NotEmpty()
                .NotNull()
                .MaximumLength(150);
        }

        protected void ValidateCity()
        {
            RuleFor(x => x.City)
                .NotEmpty()
                .NotNull()
                .MaximumLength(150);
        }

        protected void ValidateState()
        {
            RuleFor(x => x.State)
                .NotEmpty()
                .NotNull()
                .MaximumLength(2);
        }

        protected void ValidateCountry()
        {
            RuleFor(x => x.Country)
                .NotEmpty()
                .NotNull()
                .MaximumLength(5);
        }

        protected void ValidateLatitude()
        {
            RuleFor(x => x.Latitude)
                .NotEmpty()
                .NotNull()
                .MaximumLength(30);
        }

        protected void ValidateLongitude()
        {
            RuleFor(x => x.Longitude)
                .NotEmpty()
                .NotNull()
                .MaximumLength(30);
        }


    }
}
