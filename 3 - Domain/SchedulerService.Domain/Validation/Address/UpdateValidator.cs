
namespace SchedulerService.Domain.Validation.Address
{
    public class UpdateValidator : AddressValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateZipcode();
            ValidateStreet();
            ValidateNumber();
            ValidateComplement();
            ValidateNeighborhood();
            ValidateCity();
            ValidateState();
            ValidateCountry();
            ValidateLatitude();
            ValidateLongitude();

        }
    }
}
