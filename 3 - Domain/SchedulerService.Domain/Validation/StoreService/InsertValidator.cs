
namespace SchedulerService.Domain.Validation.StoreService
{
    public class InsertValidator : StoreServiceValidator
    {
        public InsertValidator()
        {
            ValidateName();
            ValidateType();
            ValidateDuration();
            ValidatePrice();
            ValidatePriceType();

        }
    }
}
