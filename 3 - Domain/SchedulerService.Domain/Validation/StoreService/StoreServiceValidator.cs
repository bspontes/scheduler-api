using FluentValidation;
using SchedulerService.Domain.Commands.StoreService;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.StoreService
{
    public class StoreServiceValidator : BaseValidation<StoreServiceCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateName()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateType()
        {
            RuleFor(x => x.StoreServiceType)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateDuration()
        {
            RuleFor(x => x.Duration)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidatePrice()
        {
            RuleFor(x => x.Price)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidatePriceType()
        {
            RuleFor(x => x.PriceType)
                .IsInEnum();
        }

        protected void ValidateStoreId()
        {
            RuleFor(x => x.StoreId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }
    }
}
