
namespace SchedulerService.Domain.Validation.StoreService
{
    public class UpdateValidator : StoreServiceValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateName();
            ValidateType();
            ValidateDuration();
            ValidatePrice();
            ValidatePriceType();
            ValidateStoreId();

        }
    }
}
