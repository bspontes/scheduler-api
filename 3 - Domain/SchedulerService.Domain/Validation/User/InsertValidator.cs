
namespace SchedulerService.Domain.Validation.User
{
    public class InsertValidator : UserValidator
    {
        public InsertValidator()
        {
            ValidateName();
            ValidatePassword();
            ValidateEmail();
            ValidateDocument();
            ValidateSex();
            ValidateProfession();
            ValidateStatus();
            ValidateProfileUrl();
            ValidateType();
            ValidateOrigin();

        }
    }
}
