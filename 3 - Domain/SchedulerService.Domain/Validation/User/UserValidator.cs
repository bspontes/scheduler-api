using FluentValidation;
using SchedulerService.Domain.Commands.User;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.User
{
    public class UserValidator : BaseValidation<UserCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateName()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidatePassword()
        {
            RuleFor(x => x.Password)
                .NotEmpty()
                .NotNull()
                .MaximumLength(20);
        }

        protected void ValidateEmail()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateDocument()
        {
            RuleFor(x => x.Document)
                .NotEmpty()
                .NotNull()
                .MaximumLength(15);
        }

        protected void ValidateSex()
        {
            RuleFor(x => x.Sex)
                .IsInEnum();

        }

        protected void ValidateProfession()
        {
            RuleFor(x => x.Profession)
                .MaximumLength(100);
        }

        protected void ValidateStatus()
        {
            RuleFor(x => x.Status)
                .IsInEnum();
        }

        protected void ValidateProfileUrl()
        {
            RuleFor(x => x.ProfileUrl)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
        }

        protected void ValidateType()
        {
            RuleFor(x => x.UserType)
                .IsInEnum();

        }

        protected void ValidateOrigin()
        {
            RuleFor(x => x.Origin)
                .IsInEnum();            
        }
    }
}
