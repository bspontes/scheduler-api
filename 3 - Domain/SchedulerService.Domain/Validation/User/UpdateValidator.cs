
namespace SchedulerService.Domain.Validation.User
{
    public class UpdateValidator : UserValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateName();
            ValidatePassword();
            ValidateEmail();
            ValidateDocument();
            ValidateSex();
            ValidateProfession();
            ValidateStatus();
            ValidateProfileUrl();
            ValidateType();
            ValidateOrigin();

        }
    }
}
