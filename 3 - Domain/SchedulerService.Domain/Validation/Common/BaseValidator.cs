﻿using FluentValidation;
using SchedulerService.Domain.Core.Commands;

namespace SchedulerService.Domain.Validation.Common
{
    public abstract class BaseValidation<T> : AbstractValidator<T> where T : Command
    {

    }
}