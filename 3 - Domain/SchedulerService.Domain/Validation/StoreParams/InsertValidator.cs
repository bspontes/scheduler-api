
namespace SchedulerService.Domain.Validation.StoreParams
{
    public class InsertValidator : StoreParamsValidator
    {
        public InsertValidator()
        {
            ValidateDescription();
            ValidateLogoUrl();
            ValidateBackgroundUrl();
            ValidateBaseColor();
            ValidateTextColor();

        }
    }
}
