namespace SchedulerService.Domain.Validation.StoreParams
{
    public class DeleteValidator : StoreParamsValidator
    {
        public DeleteValidator()
        {
            ValidateId();
        }
    }
}
