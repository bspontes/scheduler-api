
namespace SchedulerService.Domain.Validation.StoreParams
{
    public class UpdateValidator : StoreParamsValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateDescription();
            ValidateLogoUrl();
            ValidateBackgroundUrl();
            ValidateBaseColor();
            ValidateTextColor();
            ValidateStoreId();

        }
    }
}
