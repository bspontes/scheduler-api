using FluentValidation;
using SchedulerService.Domain.Commands.StoreParams;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.StoreParams
{
    public class StoreParamsValidator : BaseValidation<StoreParamsCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateDescription()
        {
            RuleFor(x => x.Description)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
        }

        protected void ValidateLogoUrl()
        {
            RuleFor(x => x.LogoUrl)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
        }

        protected void ValidateBackgroundUrl()
        {
            RuleFor(x => x.BackgroundUrl)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
        }

        protected void ValidateBaseColor()
        {
            RuleFor(x => x.BaseColor)
                .NotEmpty()
                .NotNull()
                .MaximumLength(20);
        }

        protected void ValidateTextColor()
        {
            RuleFor(x => x.TextColor)
                .NotEmpty()
                .NotNull()
                .MaximumLength(20);
        }

        protected void ValidateStoreId()
        {
            RuleFor(x => x.StoreId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }


    }
}
