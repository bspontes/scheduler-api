using FluentValidation;
using SchedulerService.Domain.Commands.Phone;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.Phone
{
    public class PhoneValidator : BaseValidation<PhoneCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateCountryCode()
        {
            RuleFor(x => x.CountryCode)
                .NotEmpty()
                .NotNull()
                .MaximumLength(3);
        }

        protected void ValidateAreaCode()
        {
            RuleFor(x => x.AreaCode)
                .NotEmpty()
                .NotNull()
                .MaximumLength(3);
        }

        protected void ValidateNumber()
        {
            RuleFor(x => x.Number)
                .NotEmpty()
                .NotNull()
                .MaximumLength(10);
        }


    }
}
