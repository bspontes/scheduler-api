
namespace SchedulerService.Domain.Validation.Phone
{
    public class UpdateValidator : PhoneValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateCountryCode();
            ValidateAreaCode();
            ValidateNumber();

        }
    }
}
