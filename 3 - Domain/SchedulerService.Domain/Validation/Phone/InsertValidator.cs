
namespace SchedulerService.Domain.Validation.Phone
{
    public class InsertValidator : PhoneValidator
    {
        public InsertValidator()
        {
            ValidateCountryCode();
            ValidateAreaCode();
            ValidateNumber();

        }
    }
}
