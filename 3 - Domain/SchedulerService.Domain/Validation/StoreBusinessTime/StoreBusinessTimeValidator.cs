using FluentValidation;
using SchedulerService.Domain.Commands.StoreBusinessTime;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.StoreBusinessTime
{
    public class StoreBusinessTimeValidator : BaseValidation<StoreBusinessTimeCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateDayOfWeek()
        {
            RuleFor(x => x.DayOfWeek)
                .NotEmpty()
                .NotNull()
                .MaximumLength(50);
        }

        protected void ValidateStartTime()
        {
            RuleFor(x => x.StartTime)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateLunchTime()
        {
            RuleFor(x => x.LunchTime)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateEndTime()
        {
            RuleFor(x => x.EndTime)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateStoreId()
        {
            RuleFor(x => x.StoreId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }


    }
}
