
namespace SchedulerService.Domain.Validation.StoreBusinessTime
{
    public class UpdateValidator : StoreBusinessTimeValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateDayOfWeek();
            ValidateStartTime();
            ValidateLunchTime();
            ValidateEndTime();
            ValidateStoreId();

        }
    }
}
