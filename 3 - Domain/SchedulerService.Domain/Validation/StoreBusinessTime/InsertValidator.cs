
namespace SchedulerService.Domain.Validation.StoreBusinessTime
{
    public class InsertValidator : StoreBusinessTimeValidator
    {
        public InsertValidator()
        {
            ValidateDayOfWeek();
            ValidateStartTime();
            ValidateLunchTime();
            ValidateEndTime();

        }
    }
}
