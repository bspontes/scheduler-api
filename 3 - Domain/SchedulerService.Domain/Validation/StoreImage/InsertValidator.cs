
namespace SchedulerService.Domain.Validation.StoreImage
{
    public class InsertValidator : StoreImageValidator
    {
        public InsertValidator()
        {
            ValidateOriginalUrl();
            ValidateThumbnailUrl();

        }
    }
}
