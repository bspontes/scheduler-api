
namespace SchedulerService.Domain.Validation.StoreImage
{
    public class UpdateValidator : StoreImageValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateOriginalUrl();
            ValidateThumbnailUrl();
            ValidateStoreId();

        }
    }
}
