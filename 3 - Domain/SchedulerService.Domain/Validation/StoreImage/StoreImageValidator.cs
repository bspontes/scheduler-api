using FluentValidation;
using SchedulerService.Domain.Commands.StoreImage;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.StoreImage
{
    public class StoreImageValidator : BaseValidation<StoreImageCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateOriginalUrl()
        {
            RuleFor(x => x.OriginalUrl)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
        }

        protected void ValidateThumbnailUrl()
        {
            RuleFor(x => x.ThumbnailUrl)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
        }

        protected void ValidateStoreId()
        {
            RuleFor(x => x.StoreId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }


    }
}
