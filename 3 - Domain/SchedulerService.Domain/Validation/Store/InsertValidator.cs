
namespace SchedulerService.Domain.Validation.Store
{
    public class InsertValidator : StoreValidator
    {
        public InsertValidator()
        {
            ValidateName();
            ValidateFantasyName();
            ValidateEmail();
            ValidateDocument();
            ValidateBusinessLine();
            ValidateStatus();

        }
    }
}
