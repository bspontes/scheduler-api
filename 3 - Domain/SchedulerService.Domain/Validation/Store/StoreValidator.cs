using FluentValidation;
using SchedulerService.Domain.Commands.Store;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.Store
{
    public class StoreValidator : BaseValidation<StoreCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateName()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateFantasyName()
        {
            RuleFor(x => x.FantasyName)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateEmail()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }

        protected void ValidateDocument()
        {
            RuleFor(x => x.Document)
                .NotEmpty()
                .NotNull()
                .MaximumLength(20);
        }

        protected void ValidateBusinessLine()
        {
            RuleFor(x => x.BusinessLine)
                .IsInEnum();
        }

        protected void ValidateStatus()
        {
            RuleFor(x => x.Status)
                .IsInEnum();
        }
    }
}
