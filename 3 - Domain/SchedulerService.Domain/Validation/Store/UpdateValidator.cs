
namespace SchedulerService.Domain.Validation.Store
{
    public class UpdateValidator : StoreValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateName();
            ValidateFantasyName();
            ValidateEmail();
            ValidateDocument();
            ValidateBusinessLine();
            ValidateStatus();

        }
    }
}
