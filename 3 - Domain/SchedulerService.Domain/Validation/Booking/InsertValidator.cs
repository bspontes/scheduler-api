
namespace SchedulerService.Domain.Validation.Booking
{
    public class InsertValidator : BookingValidator
    {
        public InsertValidator()
        {
            ValidateDay();
            ValidateStartTime();
            ValidateEndTime();

        }
    }
}
