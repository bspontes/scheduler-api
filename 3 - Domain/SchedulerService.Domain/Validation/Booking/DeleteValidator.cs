namespace SchedulerService.Domain.Validation.Booking
{
    public class DeleteValidator : BookingValidator
    {
        public DeleteValidator()
        {
            ValidateId();
        }
    }
}
