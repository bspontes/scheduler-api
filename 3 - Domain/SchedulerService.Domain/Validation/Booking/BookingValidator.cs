using FluentValidation;
using SchedulerService.Domain.Commands.Booking;
using SchedulerService.Domain.Validation.Common;

namespace SchedulerService.Domain.Validation.Booking
{
    public class BookingValidator : BaseValidation<BookingCommand>
    {
        protected void ValidateId()
        {
            RuleFor(x => x.Id)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateUserId()
        {
            RuleFor(x => x.UserId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateStoreUserId()
        {
            RuleFor(x => x.StoreUserId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateStoreId()
        {
            RuleFor(x => x.StoreId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateStoreServiceId()
        {
            RuleFor(x => x.StoreServiceId)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateDay()
        {
            RuleFor(x => x.Day)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateStartTime()
        {
            RuleFor(x => x.StartTime)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }

        protected void ValidateEndTime()
        {
            RuleFor(x => x.EndTime)
                .NotEmpty()
                .NotNull()
                .MaximumLength(40);
        }


    }
}
