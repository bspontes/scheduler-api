
namespace SchedulerService.Domain.Validation.Booking
{
    public class UpdateValidator : BookingValidator
    {
        public UpdateValidator()
        {
            ValidateId();
            ValidateUserId();
            ValidateStoreUserId();
            ValidateStoreId();
            ValidateStoreServiceId();
            ValidateDay();
            ValidateStartTime();
            ValidateEndTime();

        }
    }
}
