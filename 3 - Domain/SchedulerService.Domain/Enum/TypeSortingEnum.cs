﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum TypeSortingEnum
    {
        [EnumMember(Value = "ASC")]
        ASC,

        [EnumMember(Value = "DESC")]
        DESC,
    }
}
