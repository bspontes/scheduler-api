﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum BookingStatusEnum
    {
        [EnumMember(Value = "RESERVED")]
        RESERVED,

        [EnumMember(Value = "COMPLETED")]
        COMPLETED,

        [EnumMember(Value = "WAITING_CONFIRMATION")]
        WAITING_CONFIRMATION
    }

}
