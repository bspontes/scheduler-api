﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum UserTypeEnum
    {
        [EnumMember(Value = "CUSTOMER")]
        CUSTOMER,

        [EnumMember(Value = "PROVIDER")]
        PROVIDER,

        [EnumMember(Value = "BACKOFFICE")]
        BACKOFFICE,
    }
}
