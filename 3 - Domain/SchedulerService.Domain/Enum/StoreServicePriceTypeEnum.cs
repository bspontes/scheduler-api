﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum StoreServicePriceTypeEnum
    {
        [EnumMember(Value = "FIXED")]
        FIXED,

        [EnumMember(Value = "VARIANT")]
        VARIANT,

    }
}
