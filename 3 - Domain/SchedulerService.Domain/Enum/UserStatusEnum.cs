﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum UserStatusEnum
    {
        [EnumMember(Value = "INACTIVE")]
        INACTIVE,

        [EnumMember(Value = "ACTIVE")]
        ACTIVE,

        [EnumMember(Value = "BLOCKED")]
        BLOCKED
    }

}
