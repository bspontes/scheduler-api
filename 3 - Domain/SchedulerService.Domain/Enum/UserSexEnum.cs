﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum UserSexEnum
    {
        [EnumMember(Value = "MALE")]
        MALE,

        [EnumMember(Value = "FEMALE")]
        FEMALE,

        [EnumMember(Value = "UNDEFINED")]
        UNDEFINED,

        [EnumMember(Value = "NOT_ANSWER")]
        NOT_ANSWER,

    }
}
