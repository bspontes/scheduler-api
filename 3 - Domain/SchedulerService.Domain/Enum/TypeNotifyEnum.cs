﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum TypeNotifyEnum
    {
        [EnumMember(Value = "ERROR")]
        ERROR,

        [EnumMember(Value = "NOTIFY")]
        NOTIFY,
    }
}
