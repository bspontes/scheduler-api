﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum StoreBusinessLineEnum
    {
        [EnumMember(Value = "BEAUTY_SALON")]
        BEAUTY_SALON,

    }
}
