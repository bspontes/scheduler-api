﻿using System.Runtime.Serialization;

namespace SchedulerService.Domain.Enum
{
    public enum UserOriginEnum
    {
        [EnumMember(Value = "BASE")]
        BASE,

        [EnumMember(Value = "FACEBOOK")]
        FACEBOOK,

        [EnumMember(Value = "GMAIL")]
        GMAIL
    }

}
