﻿using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace SchedulerService.Domain.Util
{
    public static class StreamHelder
    {
        public static string ReadBodyAsString(Stream body)
        {
            string requestBody;

            using (var stream = new StreamReader(body))
            {
                stream.BaseStream.Position = 0;
                requestBody = stream.ReadToEnd();
            }

            var jObj = JsonConvert.DeserializeObject<JObject>(requestBody);
            var newResult = CodecHelper.Decrypt(jObj["data"]?.Value<string>());

            return newResult;
        }

        public static MemoryStream ToStream(object data)
        {
            var stream = new MemoryStream();

            using (var writeFile = new StreamWriter(stream, leaveOpen: true))
            {
                var csvConfiguration = new CsvConfiguration(new CultureInfo("pt-BR", false));
                var csv = new CsvWriter(writeFile, csvConfiguration);
                csv.WriteRecords((IEnumerable<dynamic>)data);
            }

            stream.Position = 0;

            return stream;
        }
    }
}