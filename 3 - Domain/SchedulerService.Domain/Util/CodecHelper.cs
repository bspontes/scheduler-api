﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Security.Cryptography;
using System.Text;

namespace SchedulerService.Domain.Util
{
    public static class CodecHelper
    {
        private static readonly RijndaelManaged MyRijndael = new RijndaelManaged();
        private static readonly int Iterations;
        private static readonly byte[] Salt;

        static CodecHelper()
        {
            MyRijndael.BlockSize = 128;
            MyRijndael.KeySize = 128;
            MyRijndael.IV = HexStringToByteArray("e84ad660c4721ae0e84ad660c4721ae0");

            MyRijndael.Padding = PaddingMode.PKCS7;
            MyRijndael.Mode = CipherMode.CBC;
            Iterations = 1000;
            Salt = Encoding.UTF8.GetBytes("insight123resultxyz");
            MyRijndael.Key = GenerateKey("801ec004103462286d8881c1fa285cf17428e1bce100af588801935ff310f2a8");
        }

        public static object Encrypt(object strPlainText)
        {
            return strPlainText;
            // try
            // {
            //     var serializer = new JsonSerializer { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            //     var obj = JObject.FromObject(strPlainText, serializer);
            //     var strText = new UTF8Encoding().GetBytes(JsonConvert.SerializeObject(obj));
            //     var transform = MyRijndael.CreateEncryptor();
            //     var cipherText = transform.TransformFinalBlock(strText, 0, strText.Length);

            //     return Convert.ToBase64String(cipherText);
            // }
            // catch (Exception e)
            // {
            //     var serializer = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            //     var obj = JsonConvert.SerializeObject(strPlainText, serializer);
            //     var strText = new UTF8Encoding().GetBytes(obj);
            //     var transform = MyRijndael.CreateEncryptor();
            //     var cipherText = transform.TransformFinalBlock(strText, 0, strText.Length);

            //     return Convert.ToBase64String(cipherText);
            // }
        }

        public static string Decrypt(string encryptedText)
        {
            if (string.IsNullOrEmpty(encryptedText)) return null;

            var encryptedBytes = Convert.FromBase64String(encryptedText);
            var decryptor = MyRijndael.CreateDecryptor(MyRijndael.Key, MyRijndael.IV);
            var originalBytes = decryptor.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);

            return Encoding.UTF8.GetString(originalBytes);
        }

        public static byte[] HexStringToByteArray(string strHex)
        {
            dynamic r = new byte[strHex.Length / 2];
            for (var i = 0; i <= strHex.Length - 1; i += 2)
            {
                r[i / 2] = Convert.ToByte(Convert.ToInt32(strHex.Substring(i, 2), 16));
            }
            return r;
        }

        private static byte[] GenerateKey(string strPassword)
        {
            var rfc2898 = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(strPassword), Salt, Iterations);

            return rfc2898.GetBytes(128 / 8);
        }
    }
}
