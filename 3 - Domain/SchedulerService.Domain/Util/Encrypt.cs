﻿using System;
using System.IO;
using System.Text;

namespace SchedulerService.Domain.Util
{
    public static class Encrypt
    {
        public static string Base64Encode(string data)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(data);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string StreamToString(Stream stream)
        {
            var length = stream.Length;
            var data = new byte[length];
            stream.Read(data, 0, (int)length);
            return Encoding.Default.GetString(data);
        }
    }
}
