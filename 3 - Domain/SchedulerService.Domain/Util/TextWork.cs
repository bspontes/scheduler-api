﻿using System;

namespace SchedulerService.Domain.Util
{
    public static class TextWork
    {
        public static string AdjustPropertyType(string name)
        {
            if (name.Contains("DateTime")) return name;

            var nameGenerated = name
                .ToLower()
                .Replace("32", string.Empty)
                .Replace("64", string.Empty);

            return nameGenerated;
        }

        public static string FirstLowerCase(string text)
        {
            if (string.IsNullOrEmpty(text)) return null;

            var textGenerated = char.ToLowerInvariant(text[0]) + text.Substring(1);

            return textGenerated;
        }

        public static string FirstUpperCase(string text)
        {
            if (string.IsNullOrEmpty(text)) return null;

            var textGenerated = char.ToLowerInvariant(text[0]) + text.Substring(1);

            return textGenerated;
        }
    }
}
