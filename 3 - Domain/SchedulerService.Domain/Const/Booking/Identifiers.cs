using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.Booking
{
    public static class Identifiers
    {
        public static string BOOKING_LIST(BookingFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"booking-{filter}-{sorting}-{pagination}";

        public static string BOOKING_DOWNLOAD(BookingFilter filter) => $"booking-download-{filter}";

        public static string BOOKING_GET(string id) => $"booking-{id}";

        public static string BOOKING_LAST_UPDATE() => $"booking-last-update";

        public static DateTime BOOKING_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
