using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.Store
{
    public static class Identifiers
    {
        public static string STORE_LIST(StoreFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"store-{filter}-{sorting}-{pagination}";

        public static string STORE_DOWNLOAD(StoreFilter filter) => $"store-download-{filter}";

        public static string STORE_GET(string id) => $"store-{id}";

        public static string STORE_LAST_UPDATE() => $"store-last-update";

        public static DateTime STORE_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
