using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.Address
{
    public static class Identifiers
    {
        public static string ADDRESS_LIST(AddressFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"address-{filter}-{sorting}-{pagination}";

        public static string ADDRESS_DOWNLOAD(AddressFilter filter) => $"address-download-{filter}";

        public static string ADDRESS_GET(string id) => $"address-{id}";

        public static string ADDRESS_LAST_UPDATE() => $"address-last-update";

        public static DateTime ADDRESS_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
