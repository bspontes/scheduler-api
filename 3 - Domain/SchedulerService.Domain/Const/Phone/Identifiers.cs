using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.Phone
{
    public static class Identifiers
    {
        public static string PHONE_LIST(PhoneFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"phone-{filter}-{sorting}-{pagination}";

        public static string PHONE_DOWNLOAD(PhoneFilter filter) => $"phone-download-{filter}";

        public static string PHONE_GET(string id) => $"phone-{id}";

        public static string PHONE_LAST_UPDATE() => $"phone-last-update";

        public static DateTime PHONE_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
