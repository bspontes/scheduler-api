using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.StoreImage
{
    public static class Identifiers
    {
        public static string STOREIMAGE_LIST(StoreImageFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"storeimage-{filter}-{sorting}-{pagination}";

        public static string STOREIMAGE_DOWNLOAD(StoreImageFilter filter) => $"storeimage-download-{filter}";

        public static string STOREIMAGE_GET(string id) => $"storeimage-{id}";

        public static string STOREIMAGE_LAST_UPDATE() => $"storeimage-last-update";

        public static DateTime STOREIMAGE_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
