using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.StoreParams
{
    public static class Identifiers
    {
        public static string STOREPARAMS_LIST(StoreParamsFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"storeparams-{filter}-{sorting}-{pagination}";

        public static string STOREPARAMS_DOWNLOAD(StoreParamsFilter filter) => $"storeparams-download-{filter}";

        public static string STOREPARAMS_GET(string id) => $"storeparams-{id}";

        public static string STOREPARAMS_LAST_UPDATE() => $"storeparams-last-update";

        public static DateTime STOREPARAMS_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
