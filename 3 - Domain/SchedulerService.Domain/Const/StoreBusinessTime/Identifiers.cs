using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.StoreBusinessTime
{
    public static class Identifiers
    {
        public static string STOREBUSINESSTIME_LIST(StoreBusinessTimeFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"storebusinesstime-{filter}-{sorting}-{pagination}";

        public static string STOREBUSINESSTIME_DOWNLOAD(StoreBusinessTimeFilter filter) => $"storebusinesstime-download-{filter}";

        public static string STOREBUSINESSTIME_GET(string id) => $"storebusinesstime-{id}";

        public static string STOREBUSINESSTIME_LAST_UPDATE() => $"storebusinesstime-last-update";

        public static DateTime STOREBUSINESSTIME_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
