using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.User
{
    public static class Identifiers
    {
        public static string USER_LIST(UserFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"user-{filter}-{sorting}-{pagination}";

        public static string USER_DOWNLOAD(UserFilter filter) => $"user-download-{filter}";

        public static string USER_GET(string id) => $"user-{id}";

        public static string USER_LAST_UPDATE() => $"user-last-update";

        public static DateTime USER_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
