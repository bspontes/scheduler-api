using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using System;

namespace SchedulerService.Domain.Const.StoreService
{
    public static class Identifiers
    {
        public static string STORESERVICE_LIST(StoreServiceFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination) => $"storeservice-{filter}-{sorting}-{pagination}";

        public static string STORESERVICE_DOWNLOAD(StoreServiceFilter filter) => $"storeservice-download-{filter}";

        public static string STORESERVICE_GET(string id) => $"storeservice-{id}";

        public static string STORESERVICE_LAST_UPDATE() => $"storeservice-last-update";

        public static DateTime STORESERVICE_TIMEOUT => DateTime.Now.AddDays(7);
    }
}
