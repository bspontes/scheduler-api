using SchedulerService.Domain.Enum;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class StoreServiceViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }

        public string StoreId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Duration { get; set; }

        public string Price { get; set; }

        public StoreServicePriceTypeEnum? PriceType { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
