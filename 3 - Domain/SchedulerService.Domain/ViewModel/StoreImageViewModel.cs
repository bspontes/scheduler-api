using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class StoreImageViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }

        public string StoreId { get; set; }
        
        public string OriginalUrl { get; set; }
        
        public string ThumbnailUrl { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
