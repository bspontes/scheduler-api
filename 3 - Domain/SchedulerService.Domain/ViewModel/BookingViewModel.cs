using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class BookingViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }

        public string UserId { get; set; }

        public string StoreUserId { get; set; }

        public string StoreId { get; set; }

        public string StoreServiceId { get; set; }

        public string Day { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
