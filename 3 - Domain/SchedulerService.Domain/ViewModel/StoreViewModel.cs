using SchedulerService.Domain.Enum;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class StoreViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }

        public string Name { get; set; }
        
        public string FantasyName { get; set; }
        
        public string Email { get; set; }
        
        public string Document { get; set; }
        
        public StoreBusinessLineEnum? BusinessLine { get; set; }
        
        public StoreStatusEnum? Status { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
