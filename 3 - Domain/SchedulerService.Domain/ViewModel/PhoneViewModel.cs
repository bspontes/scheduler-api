using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class PhoneViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }
        
        public string CountryCode { get; set; }
        
        public string AreaCode { get; set; }
        
        public string Number { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
