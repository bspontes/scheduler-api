using SchedulerService.Domain.Enum;
using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class UserViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string Document { get; set; }

        public UserSexEnum? Sex { get; set; }

        public string Profession { get; set; }

        public UserStatusEnum? Status { get; set; }

        public string ProfileUrl { get; set; }

        public UserTypeEnum? Type { get; set; }

        public UserOriginEnum? Origin { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
