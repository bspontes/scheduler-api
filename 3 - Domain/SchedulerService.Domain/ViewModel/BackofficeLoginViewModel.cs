using SchedulerService.Domain.Enum;

namespace SchedulerService.Domain.ViewModel
{
    public class BackofficeLoginViewModel
    {
        public string Email { get; set; }
        
        public string Password { get; set; }
        
        public UserTypeEnum? Type { get; set; }
        
    }
}
