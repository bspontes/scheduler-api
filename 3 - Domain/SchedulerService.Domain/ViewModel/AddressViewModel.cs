using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class AddressViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }
        
        public string Zipcode { get; set; }
        
        public string Street { get; set; }
        
        public string Number { get; set; }
        
        public string Complement { get; set; }
        
        public string Neighborhood { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }
        
        public string Country { get; set; }
        
        public string Latitude { get; set; }
        
        public string Longitude { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
