using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class StoreParamsViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }

        public string StoreId { get; set; }

        public string Description { get; set; }

        public string LogoUrl { get; set; }
        
        public string BackgroundUrl { get; set; }
        
        public string BaseColor { get; set; }
        
        public string TextColor { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
