using Swashbuckle.AspNetCore.Annotations;
using System;

namespace SchedulerService.Domain.ViewModel
{
    public class StoreBusinessTimeViewModel
    {
        [SwaggerSchema(ReadOnly = true)]
        public string Id { get; set; }

        public string StoreId { get; set; }

        public string DayOfWeek { get; set; }
        
        public string StartTime { get; set; }
        
        public string LunchTime { get; set; }
        
        public string EndTime { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; }

        [SwaggerSchema(ReadOnly = true)]
        public DateTime UpdatedAt { get; set; }
    }
}
