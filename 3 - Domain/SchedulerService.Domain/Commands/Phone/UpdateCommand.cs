using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.Phone;
using System;

namespace SchedulerService.Domain.Commands.Phone
{
    public class UpdateCommand : PhoneCommand
    {
        public UpdateCommand
            (
                string id,
                string countryCode,
                string areaCode,
                string number
            )
        {
                Id = id;
                CountryCode = countryCode;
                AreaCode = areaCode;
                Number = number;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}