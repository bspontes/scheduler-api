using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.Phone;
using System;

namespace SchedulerService.Domain.Commands.Phone
{
    public class InsertCommand : PhoneCommand
    {
        public InsertCommand
            (
                string countryCode,
                string areaCode,
                string number
            )
        {
                CountryCode = countryCode;
                AreaCode = areaCode;
                Number = number;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}