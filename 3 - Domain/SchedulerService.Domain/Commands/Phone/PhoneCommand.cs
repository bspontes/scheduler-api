using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.Phone
{
    public abstract class PhoneCommand : Command
    {
        public string Id { get; protected set; }

        public string CountryCode { get; protected set; }

        public string AreaCode { get; protected set; }

        public string Number { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}