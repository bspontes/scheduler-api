using SchedulerService.Domain.Validation.Phone;

namespace SchedulerService.Domain.Commands.Phone
{
    public class DeleteCommand : PhoneCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}