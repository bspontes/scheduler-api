using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreService;
using System;

namespace SchedulerService.Domain.Commands.StoreService
{
    public class InsertCommand : StoreServiceCommand
    {
        public InsertCommand
            (
                string name,
                string type,
                string duration,
                string price,
                StoreServicePriceTypeEnum? priceType,
                string storeId

            )
        {
                Name = name;
                StoreServiceType = type;
                Duration = duration;
                Price = price;
                PriceType = priceType;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}