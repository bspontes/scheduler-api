using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreService;
using System;

namespace SchedulerService.Domain.Commands.StoreService
{
    public class UpdateCommand : StoreServiceCommand
    {
        public UpdateCommand
            (
                string id,
                string name,
                string type,
                string duration,
                string price,
                StoreServicePriceTypeEnum? priceType,
                string storeId
            )
        {
                Id = id;
                Name = name;
                StoreServiceType = type;
                Duration = duration;
                Price = price;
                PriceType = priceType;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}