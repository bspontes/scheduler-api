using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.StoreService
{
    public abstract class StoreServiceCommand : Command
    {
        public string Id { get; protected set; }

        public string Name { get; protected set; }

        public string StoreServiceType { get; protected set; }

        public string Duration { get; protected set; }

        public string Price { get; protected set; }

        public StoreServicePriceTypeEnum? PriceType { get; protected set; }

        public string StoreId { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}