using SchedulerService.Domain.Validation.StoreService;

namespace SchedulerService.Domain.Commands.StoreService
{
    public class DeleteCommand : StoreServiceCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}