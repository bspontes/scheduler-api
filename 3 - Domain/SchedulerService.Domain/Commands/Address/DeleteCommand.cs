using SchedulerService.Domain.Validation.Address;

namespace SchedulerService.Domain.Commands.Address
{
    public class DeleteCommand : AddressCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}