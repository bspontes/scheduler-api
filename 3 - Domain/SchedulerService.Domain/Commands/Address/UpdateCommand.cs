using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.Address;
using System;

namespace SchedulerService.Domain.Commands.Address
{
    public class UpdateCommand : AddressCommand
    {
        public UpdateCommand
            (
                string id,
                string zipcode,
                string street,
                string number,
                string complement,
                string neighborhood,
                string city,
                string state,
                string country,
                string latitude,
                string longitude
            )
        {
                Id = id;
                Zipcode = zipcode;
                Street = street;
                Number = number;
                Complement = complement;
                Neighborhood = neighborhood;
                City = city;
                State = state;
                Country = country;
                Latitude = latitude;
                Longitude = longitude;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}