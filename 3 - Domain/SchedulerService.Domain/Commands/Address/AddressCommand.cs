using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.Address
{
    public abstract class AddressCommand : Command
    {
        public string Id { get; protected set; }

        public string Zipcode { get; protected set; }

        public string Street { get; protected set; }

        public string Number { get; protected set; }

        public string Complement { get; protected set; }

        public string Neighborhood { get; protected set; }

        public string City { get; protected set; }

        public string State { get; protected set; }

        public string Country { get; protected set; }

        public string Latitude { get; protected set; }

        public string Longitude { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}