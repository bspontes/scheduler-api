using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.Store;
using System;

namespace SchedulerService.Domain.Commands.Store
{
    public class UpdateCommand : StoreCommand
    {
        public UpdateCommand
            (
                string id,
                string name,
                string fantasyName,
                string email,
                string document,
                StoreBusinessLineEnum? businessLine,
                StoreStatusEnum? status
            )
        {
                Id = id;
                Name = name;
                FantasyName = fantasyName;
                Email = email;
                Document = document;
                BusinessLine = businessLine;
                Status = status;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}