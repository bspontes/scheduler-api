using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.Store
{
    public abstract class StoreCommand : Command
    {
        public string Id { get; protected set; }

        public string Name { get; protected set; }

        public string FantasyName { get; protected set; }

        public string Email { get; protected set; }

        public string Document { get; protected set; }

        public StoreBusinessLineEnum? BusinessLine { get; protected set; }

        public StoreStatusEnum? Status { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}