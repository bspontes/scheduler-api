using SchedulerService.Domain.Validation.Store;

namespace SchedulerService.Domain.Commands.Store
{
    public class DeleteCommand : StoreCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}