using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.Store;
using System;

namespace SchedulerService.Domain.Commands.Store
{
    public class InsertCommand : StoreCommand
    {
        public InsertCommand
            (
                string name,
                string fantasyName,
                string email,
                string document,
                StoreBusinessLineEnum? businessLine,
                StoreStatusEnum? status
            )
        {
                Name = name;
                FantasyName = fantasyName;
                Email = email;
                Document = document;
                BusinessLine = businessLine;
                Status = status;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}