using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.Booking;
using System;

namespace SchedulerService.Domain.Commands.Booking
{
    public class InsertCommand : BookingCommand
    {
        public InsertCommand
            (
                string day,
                string startTime,
                string endTime
            )
        {
                Day = day;
                StartTime = startTime;
                EndTime = endTime;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}