using SchedulerService.Domain.Validation.Booking;

namespace SchedulerService.Domain.Commands.Booking
{
    public class DeleteCommand : BookingCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}