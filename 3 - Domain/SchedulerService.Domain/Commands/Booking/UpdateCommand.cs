using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.Booking;
using System;

namespace SchedulerService.Domain.Commands.Booking
{
    public class UpdateCommand : BookingCommand
    {
        public UpdateCommand
            (
                string id,
                string userId,
                string storeUserId,
                string storeId,
                string storeServiceId,
                string day,
                string startTime,
                string endTime
            )
        {
                Id = id;
                UserId = userId;
                StoreUserId = storeUserId;
                StoreId = storeId;
                StoreServiceId = storeServiceId;
                Day = day;
                StartTime = startTime;
                EndTime = endTime;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}