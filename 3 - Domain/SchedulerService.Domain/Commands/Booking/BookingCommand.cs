using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.Booking
{
    public abstract class BookingCommand : Command
    {
        public string Id { get; protected set; }

        public string UserId { get; protected set; }

        public string StoreUserId { get; protected set; }

        public string StoreId { get; protected set; }

        public string StoreServiceId { get; protected set; }

        public string Day { get; protected set; }

        public string StartTime { get; protected set; }

        public string EndTime { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}