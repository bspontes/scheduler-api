using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreBusinessTime;
using System;

namespace SchedulerService.Domain.Commands.StoreBusinessTime
{
    public class InsertCommand : StoreBusinessTimeCommand
    {
        public InsertCommand
            (
                string dayOfWeek,
                string startTime,
                string lunchTime,
                string endTime,
                string storeId

            )
        {
                DayOfWeek = dayOfWeek;
                StartTime = startTime;
                LunchTime = lunchTime;
                EndTime = endTime;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}