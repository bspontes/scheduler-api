using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreBusinessTime;
using System;

namespace SchedulerService.Domain.Commands.StoreBusinessTime
{
    public class UpdateCommand : StoreBusinessTimeCommand
    {
        public UpdateCommand
            (
                string id,
                string dayOfWeek,
                string startTime,
                string lunchTime,
                string endTime,
                string storeId
            )
        {
                Id = id;
                DayOfWeek = dayOfWeek;
                StartTime = startTime;
                LunchTime = lunchTime;
                EndTime = endTime;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}