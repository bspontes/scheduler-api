using SchedulerService.Domain.Validation.StoreBusinessTime;

namespace SchedulerService.Domain.Commands.StoreBusinessTime
{
    public class DeleteCommand : StoreBusinessTimeCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}