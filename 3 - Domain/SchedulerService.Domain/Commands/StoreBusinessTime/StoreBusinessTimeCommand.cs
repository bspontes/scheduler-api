using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.StoreBusinessTime
{
    public abstract class StoreBusinessTimeCommand : Command
    {
        public string Id { get; protected set; }

        public string DayOfWeek { get; protected set; }

        public string StartTime { get; protected set; }

        public string LunchTime { get; protected set; }

        public string EndTime { get; protected set; }

        public string StoreId { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}