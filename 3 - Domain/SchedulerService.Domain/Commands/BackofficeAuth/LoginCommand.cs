using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.BackofficeAuth;

namespace SchedulerService.Domain.Commands.BackofficeAuth
{
    public class LoginCommand : BackofficeAuthCommand
    {
        public LoginCommand
            (
                string email,
                string password,
                UserTypeEnum? type
            )
        {
            Email = email;
            Password = password;
            UserType = type;
        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}