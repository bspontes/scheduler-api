using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;

namespace SchedulerService.Domain.Commands.BackofficeAuth
{
    public abstract class BackofficeAuthCommand : Command
    {
        public string Email { get; protected set; }

        public string Password { get; protected set; }

        public UserTypeEnum? UserType { get; protected set; }


    }
}