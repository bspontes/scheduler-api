using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreImage;
using System;

namespace SchedulerService.Domain.Commands.StoreImage
{
    public class InsertCommand : StoreImageCommand
    {
        public InsertCommand
            (
                string originalUrl,
                string thumbnailUrl,
                string storeId

            )
        {
                OriginalUrl = originalUrl;
                ThumbnailUrl = thumbnailUrl;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}