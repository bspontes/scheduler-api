using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreImage;
using System;

namespace SchedulerService.Domain.Commands.StoreImage
{
    public class UpdateCommand : StoreImageCommand
    {
        public UpdateCommand
            (
                string id,
                string originalUrl,
                string thumbnailUrl,
                string storeId
            )
        {
                Id = id;
                OriginalUrl = originalUrl;
                ThumbnailUrl = thumbnailUrl;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}