using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.StoreImage
{
    public abstract class StoreImageCommand : Command
    {
        public string Id { get; protected set; }

        public string OriginalUrl { get; protected set; }

        public string ThumbnailUrl { get; protected set; }

        public string StoreId { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}