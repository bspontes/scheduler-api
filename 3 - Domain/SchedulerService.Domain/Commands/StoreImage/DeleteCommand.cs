using SchedulerService.Domain.Validation.StoreImage;

namespace SchedulerService.Domain.Commands.StoreImage
{
    public class DeleteCommand : StoreImageCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}