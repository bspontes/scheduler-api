using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.User;
using System;

namespace SchedulerService.Domain.Commands.User
{
    public class InsertCommand : UserCommand
    {
        public InsertCommand
            (
                string name,
                string password,
                string email,
                string document,
                UserSexEnum? sex,
                string profession,
                UserStatusEnum? status,
                string profileUrl,
                UserTypeEnum? type,
                UserOriginEnum? origin
            )
        {
                Name = name;
                Password = password;
                Email = email;
                Document = document;
                Sex = sex;
                Profession = profession;
                Status = status;
                ProfileUrl = profileUrl;
                UserType = type;
                Origin = origin;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}