using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.User;
using System;

namespace SchedulerService.Domain.Commands.User
{
    public class UpdateCommand : UserCommand
    {
        public UpdateCommand
            (
                string id,
                string name,
                string password,
                string email,
                string document,
                UserSexEnum? sex,
                string profession,
                UserStatusEnum? status,
                string profileUrl,
                UserTypeEnum? type,
                UserOriginEnum? origin
            )
        {
                Id = id;
                Name = name;
                Password = password;
                Email = email;
                Document = document;
                Sex = sex;
                Profession = profession;
                Status = status;
                ProfileUrl = profileUrl;
                UserType = type;
                Origin = origin;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}