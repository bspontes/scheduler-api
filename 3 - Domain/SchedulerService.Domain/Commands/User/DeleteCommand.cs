using SchedulerService.Domain.Validation.User;

namespace SchedulerService.Domain.Commands.User
{
    public class DeleteCommand : UserCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}