using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.User
{
    public abstract class UserCommand : Command
    {
        public string Id { get; protected set; }

        public string Name { get; protected set; }

        public string Password { get; protected set; }

        public string Email { get; protected set; }

        public string Document { get; protected set; }

        public UserSexEnum? Sex { get; protected set; }

        public string Profession { get; protected set; }

        public UserStatusEnum? Status { get; protected set; }

        public string ProfileUrl { get; protected set; }

        public UserTypeEnum? UserType { get; protected set; }

        public UserOriginEnum? Origin { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}