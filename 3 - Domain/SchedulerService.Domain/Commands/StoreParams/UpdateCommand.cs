using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreParams;
using System;

namespace SchedulerService.Domain.Commands.StoreParams
{
    public class UpdateCommand : StoreParamsCommand
    {
        public UpdateCommand
            (
                string id,
                string description,
                string logoUrl,
                string backgroundUrl,
                string baseColor,
                string textColor,
                string storeId
            )
        {
                Id = id;
                Description = description;
                LogoUrl = logoUrl;
                BackgroundUrl = backgroundUrl;
                BaseColor = baseColor;
                TextColor = textColor;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new UpdateValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}