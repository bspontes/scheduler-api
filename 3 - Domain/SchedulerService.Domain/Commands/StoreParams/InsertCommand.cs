using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Validation.StoreParams;
using System;

namespace SchedulerService.Domain.Commands.StoreParams
{
    public class InsertCommand : StoreParamsCommand
    {
        public InsertCommand
            (
                string description,
                string logoUrl,
                string backgroundUrl,
                string baseColor,
                string textColor,
                string storeId

            )
        {
                Description = description;
                LogoUrl = logoUrl;
                BackgroundUrl = backgroundUrl;
                BaseColor = baseColor;
                TextColor = textColor;
                StoreId = storeId;

        }

        public override bool IsValid()
        {
            ValidationResult = new InsertValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}