using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Enum;
using System;

namespace SchedulerService.Domain.Commands.StoreParams
{
    public abstract class StoreParamsCommand : Command
    {
        public string Id { get; protected set; }

        public string Description { get; protected set; }

        public string LogoUrl { get; protected set; }

        public string BackgroundUrl { get; protected set; }

        public string BaseColor { get; protected set; }

        public string TextColor { get; protected set; }

        public string StoreId { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        public DateTime UpdatedAt { get; protected set; }


    }
}