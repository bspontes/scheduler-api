using SchedulerService.Domain.Validation.StoreParams;

namespace SchedulerService.Domain.Commands.StoreParams
{
    public class DeleteCommand : StoreParamsCommand
    {
        public DeleteCommand
            (
                string id
            )
        {
            Id = id;
        }

        public override bool IsValid()
        {
            ValidationResult = new DeleteValidator().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}