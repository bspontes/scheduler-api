﻿using SchedulerService.Domain.Core.Events;
using System;

namespace SchedulerService.Domain.Core.Notifications
{
    public class DomainNotification : Event
    {
        public Guid Id { get; }

        public string Key { get; }

        public object Value { get; }

        public DomainNotification(string key, object value, int type)
        {
            Id = Guid.NewGuid();
            Type = type;
            Key = key;
            Value = value;
        }
    }
}