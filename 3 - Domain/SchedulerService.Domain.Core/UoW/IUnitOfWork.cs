﻿using System;

namespace SchedulerService.Domain.Core.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        bool Commit();
    }
}