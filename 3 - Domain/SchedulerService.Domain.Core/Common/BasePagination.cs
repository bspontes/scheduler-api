﻿using System;

namespace SchedulerService.Domain.Core.Common
{
    public class BasePagination<T>
    {
        public BasePagination()
        {
            LastUpdate = DateTime.Now;
            Total = 0;
            Data = default;
        }

        public DateTime LastUpdate { get; set; }

        public int Total { get; set; }

        public T Data { get; set; }
    }
}
