﻿using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Core.Events;
using System.Threading.Tasks;

namespace SchedulerService.Domain.Core.Bus
{
    public interface IMediatorHandler
    {
        Task<object> SendCommand<T>(T command) where T : Command;

        Task RaiseEvent<T>(T @event) where T : Event;
    }
}
