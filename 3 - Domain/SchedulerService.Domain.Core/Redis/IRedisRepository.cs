﻿using SchedulerService.Domain.Core.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Domain.Core.Redis
{
    public interface IRedisRepository
    {
        Task<T> Get<T>(string identifier);

        Task<List<T>> GetList<T>(string identifier);

        Task<BasePagination<T>> GetListPagination<T>(string identifier);

        Task Save<T>(string identifier, T obj, DateTime? expiration = null);

        Task Delete(string identifier);

        Task<bool> ContainsKey(string identifier);

        Task<DateTime> GetLastUpdate(string identifier);

        Task SaveLastUpdate(string identifier, DateTime? lastUpdate = null, DateTime ? expiration = null);
    }
}
