﻿namespace SchedulerService.Domain.Core.Settings
{
    public class RedisSettings
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public int SyncTimeout { get; set; }
        public int ConnectTimeout { get; set; }
    }
}
