﻿using System.ComponentModel.DataAnnotations;
using MediatR;

namespace SchedulerService.Domain.Core.Events
{
    public abstract class Message : IRequest<object>
    {
        [Required]
        public string Action { get; set; }

        [Required]
        public string AggregateId { get; set; }

        [Required]
        public int Type { get; set; }

        protected Message()
        {
            Action = GetType().Name;
        }
    }
}