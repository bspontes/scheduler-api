﻿using System;
using System.ComponentModel.DataAnnotations;
using MediatR;

namespace SchedulerService.Domain.Core.Events
{
    public abstract class Event : Message, INotification
    {
        [Required]
        public DateTime CreationDate { get; protected set; }

        protected Event()
        {
            CreationDate = DateTime.Now;
        }
    }
}