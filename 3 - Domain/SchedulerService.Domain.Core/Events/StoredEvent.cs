﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchedulerService.Domain.Core.Events
{
    [Table("StoredEvent")]
    public class StoredEvent : Event
    {
        public StoredEvent()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.Now;
        }

        [Key]
        [Required]
        public string Id { get; set; }

        [Required]
        public string Data { get; set; }

        [Required]
        public string User { get; set; }
       
    }
}