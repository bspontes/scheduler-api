﻿using FluentValidation.Results;
using SchedulerService.Domain.Core.Events;
using System;

namespace SchedulerService.Domain.Core.Commands
{
    public abstract class Command : Message
    {
        public DateTime Timestamp { get; }

        public ValidationResult ValidationResult { get; set; }

        protected Command()
        {
            Timestamp = DateTime.Now;
        }

        public abstract bool IsValid();
    }
}