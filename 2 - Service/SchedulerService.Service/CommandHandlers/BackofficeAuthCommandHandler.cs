using MediatR;
using SchedulerService.Domain.Commands.BackofficeAuth;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Notifications;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Core.UoW;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Mapper;
using SchedulerService.Repository.Interface;
using SchedulerService.Service.CommandHandlers.Common;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerService.Service.CommandHandlers
{
    public class BackofficeAuthCommandHandler : CommandHandler, IRequestHandler<LoginCommand, object>
    {
        private readonly IUserRepository _repository;
        private readonly IMediatorHandler _mediator;
        private readonly IRedisRepository _redisRepository;
        private readonly UserMapper _mapper;

        public BackofficeAuthCommandHandler(IUserRepository repository,
                                     IUnitOfWork uow,
                                     IMediatorHandler mediator,
                                     IRedisRepository redisRepository,
                                     INotificationHandler<DomainNotification> notifications,
                                     UserMapper mapper) :
                                     base(uow, mediator, notifications)
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
            _redisRepository = redisRepository;
        }

        public Task<object> Handle(LoginCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                {
                    NotifyValidationErrors(command);
                    return Task.FromResult((object)command);
                }

                var model = _repository.Find(x => x.Email == command.Email && x.Password == command.Password && x.Type == command.UserType);

                if (model == null)
                {
                    _mediator.RaiseEvent(new DomainNotification(command.Action, "Usu�rio e/ou Senha inv�lidos", (int)TypeNotifyEnum.ERROR));
                    return Task.FromResult((object)command);
                }

                _mediator.RaiseEvent(new DomainNotification(command.Action, command, (int)TypeNotifyEnum.NOTIFY));

                var response = (object)_mapper.ViewModel(model);

                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                _mediator.RaiseEvent(new DomainNotification(command.Action, e.Message, (int)TypeNotifyEnum.ERROR));
            }

            return Task.FromResult((object)command);
        }
    }
}