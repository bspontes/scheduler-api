using SchedulerService.Domain.Commands.Address;
using SchedulerService.Domain.Const.Address;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Notifications;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Core.UoW;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Mapper;
using SchedulerService.Repository.Interface;
using SchedulerService.Service.CommandHandlers.Common;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerService.Service.CommandHandlers
{
    public class AddressCommandHandler : CommandHandler, IRequestHandler<InsertCommand, object>, IRequestHandler<UpdateCommand, object>, IRequestHandler<DeleteCommand, object>
    {
        private readonly IAddressRepository _repository;
        private readonly IMediatorHandler _mediator;
        private readonly IRedisRepository _redisRepository;
        private readonly AddressMapper _mapper;
        private readonly DateTime _expiration;

        public AddressCommandHandler(IAddressRepository repository,
                                     IUnitOfWork uow,
                                     IMediatorHandler mediator,
                                     IRedisRepository redisRepository,
                                     INotificationHandler<DomainNotification> notifications,
                                     AddressMapper mapper) :
                                     base(uow, mediator, notifications)
        {
            _repository = repository;
            _mediator = mediator;
            _mapper = mapper;
            _redisRepository = redisRepository;
            _expiration = Identifiers.ADDRESS_TIMEOUT;
        }

        public Task<object> Handle(InsertCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                {
                    NotifyValidationErrors(command);
                    return Task.FromResult((object)command);
                }

                var model = _mapper.Model(command);

                _repository.Insert(model);

                if (Commit())
                {
                    var identifier = Identifiers.ADDRESS_GET(model.Id);
                    _redisRepository.Save(identifier, model, _expiration);
                    var lastUpdateIdentifier = Identifiers.ADDRESS_LAST_UPDATE();
                    _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration);
                    _mediator.RaiseEvent(new DomainNotification(command.Action, command, (int)TypeNotifyEnum.NOTIFY));
                }

                var response = (object)_mapper.ViewModel(model);

                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                _mediator.RaiseEvent(new DomainNotification(command.Action, e.Message, (int)TypeNotifyEnum.ERROR));
            }

            return Task.FromResult((object)command);
        }

        public Task<object> Handle(UpdateCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                {
                    NotifyValidationErrors(command);
                    return Task.FromResult((object)command);
                }

                var model = _repository.Find(x => x.Id == command.Id);

                if (model == null)
                {
                    _mediator.RaiseEvent(new DomainNotification(command.Action, "Address not found", (int)TypeNotifyEnum.ERROR));
                    return Task.FromResult((object)command);
                }

                var update = _mapper.Update(model, command);

                _repository.Update(update);

                if (Commit())
                {
                    var identifier = Identifiers.ADDRESS_GET(model.Id);
                    _redisRepository.Save(identifier, model, _expiration);
                    var lastUpdateIdentifier = Identifiers.ADDRESS_LAST_UPDATE();
                    _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration);
                    _mediator.RaiseEvent(new DomainNotification(command.Action, command, (int)TypeNotifyEnum.NOTIFY));
                }

                var response = (object)_mapper.ViewModel(model);

                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                _mediator.RaiseEvent(new DomainNotification(command.Action, e.Message, (int)TypeNotifyEnum.ERROR));
            }

            return Task.FromResult((object)command);
        }

        public Task<object> Handle(DeleteCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (!command.IsValid())
                {
                    NotifyValidationErrors(command);
                    return Task.FromResult((object)command);
                }

                var model = _repository.Find(x => x.Id == command.Id);

                if (model == null)
                {
                    _mediator.RaiseEvent(new DomainNotification(command.Action, "Address not found", (int)TypeNotifyEnum.ERROR));
                    return Task.FromResult((object)command);
                }

                _repository.Delete(model);

                if (Commit())
                {
                    var identifier = Identifiers.ADDRESS_GET(model.Id);
                    _redisRepository.Delete(identifier);
                    var lastUpdateIdentifier = Identifiers.ADDRESS_LAST_UPDATE();
                    _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration);
                    _mediator.RaiseEvent(new DomainNotification(command.Action, command, (int)TypeNotifyEnum.NOTIFY));
                }

                var response = (object)_mapper.ViewModel(model);

                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                _mediator.RaiseEvent(new DomainNotification(command.Action, e.Message, (int)TypeNotifyEnum.ERROR));
            }

            return Task.FromResult((object)command);
        }
    }
}