﻿using MediatR;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Commands;
using SchedulerService.Domain.Core.Notifications;
using SchedulerService.Domain.Core.UoW;
using SchedulerService.Domain.Enum;

namespace SchedulerService.Service.CommandHandlers.Common
{
    public class CommandHandler
    {
        private readonly IUnitOfWork _uow;
        private readonly IMediatorHandler _bus;
        private readonly DomainNotificationHandler _notifications;

        public CommandHandler(IUnitOfWork uow, IMediatorHandler bus, INotificationHandler<DomainNotification> notifications)
        {
            _uow = uow;
            _notifications = (DomainNotificationHandler)notifications;
            _bus = bus;
        }

        protected void NotifyValidationErrors(Command message)
        {
            foreach (var error in message.ValidationResult.Errors)
            {
                _bus.RaiseEvent(new DomainNotification(message.Action, error.ErrorMessage, (int)TypeNotifyEnum.ERROR));
            }
        }

        public bool Commit()
        {
            if (_notifications.HasNotifications()) return false;
            if (_uow.Commit()) return true;

            _bus.RaiseEvent(new DomainNotification("Commit", "We had a problem during saving your data.", (int)TypeNotifyEnum.ERROR));
            return false;
        }
    }
}