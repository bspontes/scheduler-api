using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IStoreQueryHandler
    {
        Task<BasePaginationReturn<List<Store>>> List(StoreFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<Store>> FindById(string id);

        Task<BaseReturn<List<Store>>> Download(StoreFilter filter);
    }
}
