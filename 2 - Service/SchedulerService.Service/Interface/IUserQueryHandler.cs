using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IUserQueryHandler
    {
        Task<BasePaginationReturn<List<User>>> List(UserFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<User>> FindById(string id);

        Task<BaseReturn<List<User>>> Download(UserFilter filter);
    }
}
