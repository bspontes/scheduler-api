using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IStoreBusinessTimeQueryHandler
    {
        Task<BasePaginationReturn<List<StoreBusinessTime>>> List(StoreBusinessTimeFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreBusinessTime>> FindById(string id);

        Task<BaseReturn<List<StoreBusinessTime>>> Download(StoreBusinessTimeFilter filter);
    }
}
