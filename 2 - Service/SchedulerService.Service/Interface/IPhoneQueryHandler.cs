using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IPhoneQueryHandler
    {
        Task<BasePaginationReturn<List<Phone>>> List(PhoneFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<Phone>> FindById(string id);

        Task<BaseReturn<List<Phone>>> Download(PhoneFilter filter);
    }
}
