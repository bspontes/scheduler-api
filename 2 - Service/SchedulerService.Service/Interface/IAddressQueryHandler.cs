using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IAddressQueryHandler
    {
        Task<BasePaginationReturn<List<Address>>> List(AddressFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<Address>> FindById(string id);

        Task<BaseReturn<List<Address>>> Download(AddressFilter filter);
    }
}
