using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IStoreParamsQueryHandler
    {
        Task<BasePaginationReturn<List<StoreParams>>> List(StoreParamsFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreParams>> FindById(string id);

        Task<BaseReturn<List<StoreParams>>> Download(StoreParamsFilter filter);
    }
}
