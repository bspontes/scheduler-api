using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IStoreImageQueryHandler
    {
        Task<BasePaginationReturn<List<StoreImage>>> List(StoreImageFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreImage>> FindById(string id);

        Task<BaseReturn<List<StoreImage>>> Download(StoreImageFilter filter);
    }
}
