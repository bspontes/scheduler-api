using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IStoreServiceQueryHandler
    {
        Task<BasePaginationReturn<List<StoreService>>> List(StoreServiceFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<StoreService>> FindById(string id);

        Task<BaseReturn<List<StoreService>>> Download(StoreServiceFilter filter);
    }
}
