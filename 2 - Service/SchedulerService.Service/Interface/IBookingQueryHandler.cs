using SchedulerService.Domain.Common;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchedulerService.Service.Interface
{
    public interface IBookingQueryHandler
    {
        Task<BasePaginationReturn<List<Booking>>> List(BookingFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination);

        Task<BaseReturn<Booking>> FindById(string id);

        Task<BaseReturn<List<Booking>>> Download(BookingFilter filter);
    }
}
