using SchedulerService.Domain.Common;
using SchedulerService.Domain.Const.Address;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.Model;
using SchedulerService.Repository.Interface;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerService.Service.QueryHandlers
{
    public class AddressQueryHandler : IAddressQueryHandler
    {
        private readonly IAddressRepository _repository;
        private readonly IRedisRepository _redisRepository;
        private readonly AddressMapper _mapper;
        private readonly DateTime _expiration;

        public AddressQueryHandler(IAddressRepository repository, IRedisRepository redisRepository, AddressMapper mapper)
        {
            _repository = repository;
            _redisRepository = redisRepository;
            _mapper = mapper;
            _expiration = Identifiers.ADDRESS_TIMEOUT;
        }

        public async Task<BasePaginationReturn<List<Address>>> List(AddressFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<Address>>();

            try
            {
                var lastUpdateIdentifier = Identifiers.ADDRESS_LAST_UPDATE();
                var lastUpdate = await _redisRepository.GetLastUpdate(lastUpdateIdentifier);

                var identifier = Identifiers.ADDRESS_LIST(filter, sorting, pagination);
                var model = await _redisRepository.GetListPagination<List<Address>>(identifier);

                if (model.Data == null || lastUpdate > model.LastUpdate)
                {
                    var filtered = _mapper.Filter(filter);
                    model.Data = await _repository.ListAsync(filtered, sorting, pagination);
                    model.Total = await _repository.CountAsync(filtered);
                    model.LastUpdate = lastUpdate;
                }

                await _redisRepository.Save(identifier, model, _expiration);
                await _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration, lastUpdate);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<Address>> FindById(string id)
        {
            var response = new BaseReturn<Address>();

            try
            {
                var identifier = Identifiers.ADDRESS_GET(id);
                var model = await _redisRepository.Get<Address>(identifier);

                if (model == null)
                {
                    model = await _repository.FindAsync(x => x.Id == id);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<Address>>> Download(AddressFilter filter)
        {
            var response = new BaseReturn<List<Address>>();

            try
            {
                var identifier = Identifiers.ADDRESS_DOWNLOAD(filter);
                var model = await _redisRepository.GetList<Address>(identifier);

                if (!model.Any())
                {
                    var filtered = _mapper.Filter(filter);
                    model = await _repository.ListAsync(filtered);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }
    }
}
