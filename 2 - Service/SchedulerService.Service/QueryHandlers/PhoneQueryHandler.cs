using SchedulerService.Domain.Common;
using SchedulerService.Domain.Const.Phone;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.Model;
using SchedulerService.Repository.Interface;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerService.Service.QueryHandlers
{
    public class PhoneQueryHandler : IPhoneQueryHandler
    {
        private readonly IPhoneRepository _repository;
        private readonly IRedisRepository _redisRepository;
        private readonly PhoneMapper _mapper;
        private readonly DateTime _expiration;

        public PhoneQueryHandler(IPhoneRepository repository, IRedisRepository redisRepository, PhoneMapper mapper)
        {
            _repository = repository;
            _redisRepository = redisRepository;
            _mapper = mapper;
            _expiration = Identifiers.PHONE_TIMEOUT;
        }

        public async Task<BasePaginationReturn<List<Phone>>> List(PhoneFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<Phone>>();

            try
            {
                var lastUpdateIdentifier = Identifiers.PHONE_LAST_UPDATE();
                var lastUpdate = await _redisRepository.GetLastUpdate(lastUpdateIdentifier);

                var identifier = Identifiers.PHONE_LIST(filter, sorting, pagination);
                var model = await _redisRepository.GetListPagination<List<Phone>>(identifier);

                if (model.Data == null || lastUpdate > model.LastUpdate)
                {
                    var filtered = _mapper.Filter(filter);
                    model.Data = await _repository.ListAsync(filtered, sorting, pagination);
                    model.Total = await _repository.CountAsync(filtered);
                    model.LastUpdate = lastUpdate;
                }

                await _redisRepository.Save(identifier, model, _expiration);
                await _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration, lastUpdate);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<Phone>> FindById(string id)
        {
            var response = new BaseReturn<Phone>();

            try
            {
                var identifier = Identifiers.PHONE_GET(id);
                var model = await _redisRepository.Get<Phone>(identifier);

                if (model == null)
                {
                    model = await _repository.FindAsync(x => x.Id == id);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<Phone>>> Download(PhoneFilter filter)
        {
            var response = new BaseReturn<List<Phone>>();

            try
            {
                var identifier = Identifiers.PHONE_DOWNLOAD(filter);
                var model = await _redisRepository.GetList<Phone>(identifier);

                if (!model.Any())
                {
                    var filtered = _mapper.Filter(filter);
                    model = await _repository.ListAsync(filtered);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }
    }
}
