using SchedulerService.Domain.Common;
using SchedulerService.Domain.Const.User;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.Model;
using SchedulerService.Repository.Interface;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerService.Service.QueryHandlers
{
    public class UserQueryHandler : IUserQueryHandler
    {
        private readonly IUserRepository _repository;
        private readonly IRedisRepository _redisRepository;
        private readonly UserMapper _mapper;
        private readonly DateTime _expiration;

        public UserQueryHandler(IUserRepository repository, IRedisRepository redisRepository, UserMapper mapper)
        {
            _repository = repository;
            _redisRepository = redisRepository;
            _mapper = mapper;
            _expiration = Identifiers.USER_TIMEOUT;
        }

        public async Task<BasePaginationReturn<List<User>>> List(UserFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<User>>();

            try
            {
                var lastUpdateIdentifier = Identifiers.USER_LAST_UPDATE();
                var lastUpdate = await _redisRepository.GetLastUpdate(lastUpdateIdentifier);

                var identifier = Identifiers.USER_LIST(filter, sorting, pagination);
                var model = await _redisRepository.GetListPagination<List<User>>(identifier);

                if (model.Data == null || lastUpdate > model.LastUpdate)
                {
                    var filtered = _mapper.Filter(filter);
                    model.Data = await _repository.ListAsync(filtered, sorting, pagination);
                    model.Total = await _repository.CountAsync(filtered);
                    model.LastUpdate = lastUpdate;
                }

                await _redisRepository.Save(identifier, model, _expiration);
                await _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration, lastUpdate);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<User>> FindById(string id)
        {
            var response = new BaseReturn<User>();

            try
            {
                var identifier = Identifiers.USER_GET(id);
                var model = await _redisRepository.Get<User>(identifier);

                if (model == null)
                {
                    model = await _repository.FindAsync(x => x.Id == id);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<User>>> Download(UserFilter filter)
        {
            var response = new BaseReturn<List<User>>();

            try
            {
                var identifier = Identifiers.USER_DOWNLOAD(filter);
                var model = await _redisRepository.GetList<User>(identifier);

                if (!model.Any())
                {
                    var filtered = _mapper.Filter(filter);
                    model = await _repository.ListAsync(filtered);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }
    }
}
