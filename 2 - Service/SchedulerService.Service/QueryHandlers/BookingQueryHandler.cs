using SchedulerService.Domain.Common;
using SchedulerService.Domain.Const.Booking;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.Model;
using SchedulerService.Repository.Interface;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerService.Service.QueryHandlers
{
    public class BookingQueryHandler : IBookingQueryHandler
    {
        private readonly IBookingRepository _repository;
        private readonly IRedisRepository _redisRepository;
        private readonly BookingMapper _mapper;
        private readonly DateTime _expiration;

        public BookingQueryHandler(IBookingRepository repository, IRedisRepository redisRepository, BookingMapper mapper)
        {
            _repository = repository;
            _redisRepository = redisRepository;
            _mapper = mapper;
            _expiration = Identifiers.BOOKING_TIMEOUT;
        }

        public async Task<BasePaginationReturn<List<Booking>>> List(BookingFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<Booking>>();

            try
            {
                var lastUpdateIdentifier = Identifiers.BOOKING_LAST_UPDATE();
                var lastUpdate = await _redisRepository.GetLastUpdate(lastUpdateIdentifier);

                var identifier = Identifiers.BOOKING_LIST(filter, sorting, pagination);
                var model = await _redisRepository.GetListPagination<List<Booking>>(identifier);

                if (model.Data == null || lastUpdate > model.LastUpdate)
                {
                    var filtered = _mapper.Filter(filter);
                    model.Data = await _repository.ListAsync(filtered, sorting, pagination);
                    model.Total = await _repository.CountAsync(filtered);
                    model.LastUpdate = lastUpdate;
                }

                await _redisRepository.Save(identifier, model, _expiration);
                await _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration, lastUpdate);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<Booking>> FindById(string id)
        {
            var response = new BaseReturn<Booking>();

            try
            {
                var identifier = Identifiers.BOOKING_GET(id);
                var model = await _redisRepository.Get<Booking>(identifier);

                if (model == null)
                {
                    model = await _repository.FindAsync(x => x.Id == id);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<Booking>>> Download(BookingFilter filter)
        {
            var response = new BaseReturn<List<Booking>>();

            try
            {
                var identifier = Identifiers.BOOKING_DOWNLOAD(filter);
                var model = await _redisRepository.GetList<Booking>(identifier);

                if (!model.Any())
                {
                    var filtered = _mapper.Filter(filter);
                    model = await _repository.ListAsync(filtered);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }
    }
}
