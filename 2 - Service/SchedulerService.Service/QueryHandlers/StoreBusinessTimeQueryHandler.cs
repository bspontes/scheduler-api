using SchedulerService.Domain.Common;
using SchedulerService.Domain.Const.StoreBusinessTime;
using SchedulerService.Domain.Core.Redis;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.Mapper;
using SchedulerService.Domain.Model;
using SchedulerService.Repository.Interface;
using SchedulerService.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SchedulerService.Service.QueryHandlers
{
    public class StoreBusinessTimeQueryHandler : IStoreBusinessTimeQueryHandler
    {
        private readonly IStoreBusinessTimeRepository _repository;
        private readonly IRedisRepository _redisRepository;
        private readonly StoreBusinessTimeMapper _mapper;
        private readonly DateTime _expiration;

        public StoreBusinessTimeQueryHandler(IStoreBusinessTimeRepository repository, IRedisRepository redisRepository, StoreBusinessTimeMapper mapper)
        {
            _repository = repository;
            _redisRepository = redisRepository;
            _mapper = mapper;
            _expiration = Identifiers.STOREBUSINESSTIME_TIMEOUT;
        }

        public async Task<BasePaginationReturn<List<StoreBusinessTime>>> List(StoreBusinessTimeFilter filter, BaseSortingRequest sorting, BasePaginationRequest pagination)
        {
            var response = new BasePaginationReturn<List<StoreBusinessTime>>();

            try
            {
                var lastUpdateIdentifier = Identifiers.STOREBUSINESSTIME_LAST_UPDATE();
                var lastUpdate = await _redisRepository.GetLastUpdate(lastUpdateIdentifier);

                var identifier = Identifiers.STOREBUSINESSTIME_LIST(filter, sorting, pagination);
                var model = await _redisRepository.GetListPagination<List<StoreBusinessTime>>(identifier);

                if (model.Data == null || lastUpdate > model.LastUpdate)
                {
                    var filtered = _mapper.Filter(filter);
                    model.Data = await _repository.ListAsync(filtered, sorting, pagination);
                    model.Total = await _repository.CountAsync(filtered);
                    model.LastUpdate = lastUpdate;
                }

                await _redisRepository.Save(identifier, model, _expiration);
                await _redisRepository.SaveLastUpdate(lastUpdateIdentifier, _expiration, lastUpdate);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<StoreBusinessTime>> FindById(string id)
        {
            var response = new BaseReturn<StoreBusinessTime>();

            try
            {
                var identifier = Identifiers.STOREBUSINESSTIME_GET(id);
                var model = await _redisRepository.Get<StoreBusinessTime>(identifier);

                if (model == null)
                {
                    model = await _repository.FindAsync(x => x.Id == id);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }

        public async Task<BaseReturn<List<StoreBusinessTime>>> Download(StoreBusinessTimeFilter filter)
        {
            var response = new BaseReturn<List<StoreBusinessTime>>();

            try
            {
                var identifier = Identifiers.STOREBUSINESSTIME_DOWNLOAD(filter);
                var model = await _redisRepository.GetList<StoreBusinessTime>(identifier);

                if (!model.Any())
                {
                    var filtered = _mapper.Filter(filter);
                    model = await _repository.ListAsync(filtered);
                }

                await _redisRepository.Save(identifier, model, _expiration);

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return response;
        }
    }
}
