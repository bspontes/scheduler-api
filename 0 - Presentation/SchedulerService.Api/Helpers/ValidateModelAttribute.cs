﻿using SchedulerService.Domain.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace SchedulerService.Api.Helpers
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var errors = context.ModelState.Values.SelectMany(v => v.Errors);

                var response = new BaseReturn<object>
                {
                    Data = errors,
                    Success = false
                };

                context.Result = new BadRequestObjectResult(response);
            }
        }
    }
}
