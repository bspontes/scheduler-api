﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace SchedulerService.Api.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class ValidlAccessAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var url = filterContext.HttpContext.Request.RequestContext.HttpContext.Request.Url;
            //var host = url == null ? string.Empty : url.Host.Split('.')[0].ToUpper();

            //if (host != UserManager<>.UserProfile.Host)
            //{
            //    filterContext.HttpContext.Response.Redirect("/Account/LogOff", true);
            //}

            base.OnActionExecuting(filterContext);
        }
    }
}