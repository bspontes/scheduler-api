using SchedulerService.Api.Helpers;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Notifications;
using SchedulerService.Domain.Enum;
using SchedulerService.Domain.Util;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace SchedulerService.Api.Controllers
{
    [Produces("application/json")]
    [ValidateModel]
    [ProducesResponseType(typeof(BaseReturn<object>), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(BaseReturn<object>), (int)HttpStatusCode.InternalServerError)]
    public class BaseController : ControllerBase
    {
        private readonly DomainNotificationHandler _notifications;
        private readonly IMediatorHandler _mediator;

        protected BaseController(INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator)
        {
            _notifications = (DomainNotificationHandler)notifications;
            _mediator = mediator;
        }

        protected IEnumerable<DomainNotification> Notifications => _notifications.GetNotifications();

        protected bool IsValidOperation()
        {
            return (!_notifications.HasNotifications());
        }

        public IActionResult HandleResponse(BaseReturn<object> result)
        {
            try
            {
                var valid = IsValidOperation() && result.Success;

                if (!valid)
                {
                    return BadRequest(new
                    {
                        success = false,
                        data = result.Data,
                        description = Notifications?.FirstOrDefault()?.Value
                    });
                }

                return Ok(new
                {
                    success = true,
                    data = result.Data,
                    description = result.Description
                });
            }
            catch (Exception)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new
                    {
                        success = false,
                        data = result,
                        description = "Ocorreu uma falha interna no servidor."
                    });
            }
        }

        public IActionResult HandleDownload(BaseReturn<object> result, string fileName)
        {
            try
            {
                var valid = IsValidOperation() && result.Success;

                if (!valid)
                {
                    return BadRequest(new
                    {
                        success = false,
                        data = result.Data,
                        description = Notifications?.FirstOrDefault()?.Value
                    });
                }

                var stream = StreamHelder.ToStream(result.Data);

                return File(stream, "application/octet-stream", $"{fileName ?? "Reports"}-{DateTime.Now:mm-dd-yy hh-mm-ss}.csv");
            }
            catch (Exception e)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new
                    {
                        success = false,
                        data = e.Message,
                        description = "Ocorreu uma falha interna no servidor."
                    });
            }
        }

        protected void NotifyModelStateErrors()
        {
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            foreach (var error in errors)
            {
                var description = error.Exception == null ? error.ErrorMessage : error.Exception.Message;
                NotifyError(string.Empty, description);
            }
        }

        protected void NotifyError(string code, string message)
        {
            _mediator.RaiseEvent(new DomainNotification(code, message, (int)TypeNotifyEnum.ERROR));
        }
    }
}