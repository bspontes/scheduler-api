using SchedulerService.Application.Interface;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Notifications;
using SchedulerService.Domain.Filter;
using SchedulerService.Domain.ViewModel;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace SchedulerService.Api.Controllers
{
    [Route("api/store")]
    public class StoreController : BaseController
    {
        private readonly IStoreApplication _application;

        public StoreController(
            IMediatorHandler mediator,
            INotificationHandler<DomainNotification> notifications,
            IStoreApplication application) : base(notifications, mediator)
        {
            _application = application;
        }

        [HttpGet]
        [ProducesResponseType(typeof(BasePaginationReturn<List<StoreViewModel>>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> List([FromQuery] StoreFilter filter, [FromQuery] BaseSortingRequest sorting, [FromQuery] BasePaginationRequest pagination)
        {
            var response = new BaseReturn<object>();

            try
            {
                var service = await _application.List(filter, sorting, pagination);

                response.Data = service.Data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return HandleResponse(response);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BaseReturn<List<StoreViewModel>>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetById([FromRoute] string id)
        {
            var response = new BaseReturn<object>();

            try
            {
                var service = await _application.FindById(id);

                response.Data = service.Data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return HandleResponse(response);
        }

        [HttpPost]
        [ProducesResponseType(typeof(BaseReturn<object>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Insert([FromBody] StoreViewModel viewModel)
        {
            var response = new BaseReturn<object>();

            try
            {
                var service = await _application.Insert(viewModel);

                response.Data = service.Data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return HandleResponse(response);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(typeof(BaseReturn<object>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] StoreViewModel viewModel)
        {
            var response = new BaseReturn<object>();

            try
            {
                var service = await _application.Update(viewModel, id);

                response.Data = service.Data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return HandleResponse(response);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(BaseReturn<object>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var response = new BaseReturn<object>();

            try
            {
                var service = await _application.Delete(id);

                response.Data = service.Data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return HandleResponse(response);
        }


        [HttpGet("download")]
        [ProducesResponseType(typeof(BaseReturn<List<StoreViewModel>>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Download([FromQuery] StoreFilter filter)
        {
            var response = new BaseReturn<object>();

            try
            {
                var service = await _application.Download(filter);

                response.Data = service.Data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return HandleDownload(response, "Lojas");
        }
    }
}