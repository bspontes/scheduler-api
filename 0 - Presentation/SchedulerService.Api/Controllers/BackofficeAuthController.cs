using MediatR;
using Microsoft.AspNetCore.Mvc;
using SchedulerService.Application.Interface;
using SchedulerService.Domain.Common;
using SchedulerService.Domain.Core.Bus;
using SchedulerService.Domain.Core.Notifications;
using SchedulerService.Domain.ViewModel;
using System;
using System.Net;
using System.Threading.Tasks;

namespace SchedulerService.Api.Controllers
{
    [Route("api/backoffice/auth")]
    public class BackofficeAuthController : BaseController
    {
        private readonly IBackofficeAuthApplication _application;

        public BackofficeAuthController(
            IMediatorHandler mediator,
            INotificationHandler<DomainNotification> notifications,
            IBackofficeAuthApplication application) : base(notifications, mediator)
        {
            _application = application;
        }


        [HttpPost]
        [ProducesResponseType(typeof(BaseReturn<object>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Login([FromBody] BackofficeLoginViewModel viewModel)
        {
            var response = new BaseReturn<object>();

            try
            {
                var service = await _application.Login(viewModel);

                response.Data = service.Data;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Description = ex.Message;
            }

            return HandleResponse(response);
        }
    }
}